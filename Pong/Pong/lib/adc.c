/*
 * adc.c
 *
 * Created: 08.10.2011 16:12:39
 *  Author: busser.michael
 */ 

#include "avr/io.h"
#include <inttypes.h>

#include "adc.h"


void init_adc( void ) {
 
  uint16_t result;
 
  ADMUX   = (0<<REFS1) | (1<<REFS0);	    // use AVcc as reference
  //ADMUX   = (1<<REFS1) | (1<<REFS0);        // use internal reference voltage (req. input signal scaling)
  ADCSRA  = (1<<ADPS2) | (1<<ADPS1);		// derive converter freq. from system clock div 64, should be max 200kHz for best accuracy
  ADCSRA |= (1<<ADEN);						// activate the converter
 
  /* read one value after activating the adc to allow it to come up */
 
  ADCSRA |= (1<<ADSC);                  // get the very first value after activating the device
  while (ADCSRA & (1<<ADSC) ) {}        // wait for conversion to be done
  result = ADCW;						// must be read once otherwise the result of the following conversions will not be taken
}


uint16_t read_adc( uint8_t channel ) {
  ADMUX = (ADMUX & ~(0x1F)) | (channel & 0x1F);		// select input channel
  ADCSRA |= (1<<ADSC);								// do a "single conversion"
  while (ADCSRA & (1<<ADSC) ) {}					// wait until done
  return ADCW;										// readout converter and return value
}