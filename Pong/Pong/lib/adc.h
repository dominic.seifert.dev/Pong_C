/*
 * adc.h
 *
 * Created: 08.10.2011 16:13:00
 *  Author: busser.michael
 */ 


#ifndef ADC_H_
#define ADC_H_


void     init_adc( void );
uint16_t read_adc( uint8_t channel );

#endif /* ADC_H_ */