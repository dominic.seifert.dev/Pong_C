/*
 * key.c
 *
 * Created: 04.08.2014 11:52:38
 *  Author: busser.michael
 */ 


#include "key.h"


volatile uint8_t key_state;		// entprellter und invertierter Tastenzustand: bit = 1: Taste gedr�ckt
volatile uint8_t key_press;		// Tastendruck erkennen
volatile uint8_t key_rpt;		// langer Tastendruck und Repeat-Funktion



void	key_init() {
	KEY_DDR		&= ~ALL_KEYS;		//setzt die Portbits auf Eingang, die als KEY definiert wurden
	KEY_PORT	|= KEY_PULLUP;		//und nur diese PullUps einschalten, die ben�tigt werden (Inkrementlgeber hat eigene)
}

/**********************************************************************************
 * In der Originalversion h�ngt diese Funktion in einem Timer-Interrupt.
 * Hier ist sie ausgegliedert und muss in regelm�ssigen Abst�nden aufgerufen werden.
 * Das Aufrufintervall sollte 10ms sein.
 *
 *
 *
 * Ausf�hrungszeit: ca. 3,5 �s @ 16MHz
*/

void getKeyState( void ) {              // every 10ms
	static uint8_t ct0, ct1, rpt;
	uint8_t i;
	
	i = key_state ^ ~KEY_PIN;                       // key changed ?
	ct0 = ~( ct0 & i );                             // reset or count ct0
	ct1 = ct0 ^ (ct1 & i);                          // reset or count ct1
	i &= ct0 & ct1;                                 // count until roll over ?
	key_state ^= i;                                 // then toggle debounced state
	key_press |= key_state & i;                     // 0->1: key press detect
	
	if( (key_state & REPEAT_MASK) == 0 )            // check repeat function
	rpt = REPEAT_START;			                    // start delay
	if( --rpt == 0 ){
		rpt = REPEAT_NEXT;                          // repeat delay
		key_rpt |= key_state & REPEAT_MASK;
	}
}


/**********************************************************************************
 * Pr�fen, ob eine Taste gedr�ckt wurde.
 * Jeder Tastendruck wird nur einmal gemeldet. 
*/
uint8_t get_key_press( uint8_t key_mask )
{
	cli();                                          // read and clear atomic !
	key_mask &= key_press;                          // read key(s)
	key_press ^= key_mask;                          // clear key(s)
	sei();
	return key_mask;
}

/**********************************************************************************
* Pr�fen, ob eine Taste lange genug gedr�ckt war, soda� die Repeat-Funktion angesto�en wird.
* 
*
 check if a key has been pressed long enough such that the
 key repeat functionality kicks in. After a small setup delay
 the key is reported being pressed in subsequent calls
 to this function. This simulates the user repeatedly
 pressing and releasing the key.
*/
uint8_t get_key_rpt( uint8_t key_mask )
{
	cli();                                          // read and clear atomic !
	key_mask &= key_rpt;                            // read key(s)
	key_rpt ^= key_mask;                            // clear key(s)
	sei();
	return key_mask;
}

///////////////////////////////////////////////////////////////////
//
// check if a key is pressed right now
//
uint8_t get_key_state( uint8_t key_mask )

{
	key_mask &= key_state;
	return key_mask;
}

///////////////////////////////////////////////////////////////////
//
uint8_t get_key_short( uint8_t key_mask )
{
	cli();                                          // read key state and key press atomic !
	return get_key_press( ~key_state & key_mask );
}

///////////////////////////////////////////////////////////////////
//
uint8_t get_key_long( uint8_t key_mask )
{
	return get_key_press( get_key_rpt( key_mask ));
}




