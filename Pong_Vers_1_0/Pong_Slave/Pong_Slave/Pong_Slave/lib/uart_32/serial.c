/*
  
  Dieses Modul erlaubt das Senden und empfangen von Nachrichten im Format TMsg �ber den seriellen Port.
  Die �bertragung selbst erfolgt vollst�ndig Interrupt-gesteuert.
  
  Der Rahmen (Frame)

  STX |	Container f�r die Nachricht | ETX

  STX	= Start of Transmission		0x02
  ETX	= End of Transmission		0x03
  DLE	= Data Link Escape			0x1F

  Der �bertragungsschicht wird eine Nachricht im nachfolgend beschriebenen Format �bergeben. 
  Die �bertragungsschicht sendet zun�chst ein STX, um den Begin einer �bertragung anzuzeigen. 
  Am Ende einer �bertragung wird der ETX gesendet. Die Zeichen STX und ETX d�rfen im restlichen Zeichenstrom 
  nicht mehr vorkommen. Kommen sie doch vor, so werden sie durch voranstellen des Maskierungszeichens DLE maskiert. 
  Der Empf�nger entfernt das Maskierungszeichen aus dem Datenstrom und f�gt das direkt nachfolgende Zeichen dem 
  bisher empfangenen Teil des Telegrammes an.
  
  Mu� das Maskierungszeichens selbst �bertragen werden, so wird auch ihm ein Maskierungszeichen DLE vorangestellt.
  In Anlehnung an  https://en.wikipedia.org/wiki/Binary_Synchronous_Communications

  Die Schnittstelle wird durch die folgenden Funktionen gebildet:
  
	void    initSerial( void );
	void    sendMsg( TDataBuf msg );
	uint8_t peekMsg( TDataBuf *msg );
  
  An dieser Schnittstelle ist der Mechanismus des Maskierens bereits abgehandelt. Hier werden nur Nachrichten f�r den 
  Container der Transportschicht entgegengenommen oder zur�ckgeliefert.
  
  Es gibt nur einen Sendepuffer aber zwei empfangspuffer, die abwechselnd verwendet werden. 
  So kann die Auswertung eienr Nachricht andauern, w�hrend eine weitere Nachricht bereits eintrifft.


  Das Modul soll m�glichst unabh�ngig vom verwendeten Protokoll sein.	
  Aus der Protokolldefinition protocol.h wird nur die Funktion ownAdr() ben�tigt, 
  welche die eigene Busadresse des Knotens zur�ckliefert.

  F�r 2-Draht Bussysteme wie RS485 ben�tigt man noch ein Steuersignal, welches von Senden auf Empfangen umschaltet.
  Die Grundeinstellung ist Empfangen. Der Sender wrd nur f�r die Dauer der �bertragung eingeschaltet.
  Da der Empf�nger gleichzeitig mitl�uft, empf�ngt er auch die selbst gesendete Nachricht.
  Man k�nnte diesen Umstand zur Erkennung von Buskollisionen verwenden.

*/

#include <avr\io.h>
#include <avr\interrupt.h>
#include <util\delay.h>
#include <inttypes.h>

#include "serial.h"
#include "protocol.h"


//------------------------------------------------------------------------- 
// private
//------------------------------------------------------------------------- 

//- Strukturen f�r zu empfangende Daten
TDataBuf 	RxBuf[2];			//- Empfangspuffer f�r 2 Nachrichten, 
TRecCtrl	RxCtrl[2];			//- Empfangspuffer f�r 2 Nachrichten
uint8_t		RxBufIdx = 0;		//- Zeiger auf den gerade verwendeten Empfangspuffer

//- Strukturen f�r zu sendende Daten
TDataBuf 	TxBuf;				//- ein Sendepuffer
TSendCtrl	TxCtrl;				//- Steuerungsblock f�r das Senden

//- Statusflags f�r das Byte-Stuffing, also das Maskieren von Nutzdaten, welche im Rahmenaufbau eine besondere Bedeutung besitzen
uint8_t		dle_status = 0;		//- wenn ein dle gesendet oder empfangen wird

//- Makros
#define SET_DLE_RX		(dle_status |= DLE_RECEIVED)
#define CLEAR_DLE_RX	(dle_status	&= (~(DLE_RECEIVED)))
#define IS_DLE_RX		(dle_status & DLE_RECEIVED )

#define SET_DLE_TX		(dle_status |= DLE_TRANSMITTED)
#define CLEAR_DLE_TX	(dle_status	&= (~(DLE_TRANSMITTED)))
#define IS_DLE_TX		(dle_status & DLE_TRANSMITTED )

#define SEND_DLE(a)		( ((a) == FRAME_STX) || ((a) == FRAME_ETX) || ((a) == FRAME_DLE) )


//---------------------------------------------------------------------
/**
@brief	L�scht den Sendestatus:
		- Indexz�hler zur�cksetzen
		- Sendestufe aus
		- Nachricht im Buffer als ung�ltig markieren; -> bereit f�r n�chste Nachricht
		
@return	void		
*/
void clearTxCtrl( void ) {
	TxCtrl.Index     = 0;			//- Indexz�hler zur�cksetzen
	TxCtrl.bSending  = 0;			//- Sendestufe aus
	TxCtrl.bFraming  = 0;			//- kein Framingzeichen gesendet
 	TxCtrl.bMsgValid = 0;			//- Nachricht im Buffer ist ung�ltig; -> bereit f�r n�chste Nachricht
}
//---------------------------------------------------------------------
/**
@brief	L�scht den Empfangsstatus
		- Indexz�hler zur�cksetzen
		- kein laufender Empfang
		- Nachricht im Buffer als ung�ltig markieren

@param	idx Gibt an, welcher der beiden Puffer gel�scht werden soll [0|1]
		
@return	void		
*/void clearRxCtrl( uint8_t idx ) {
	RxCtrl[idx].Index        = 0;			//- Indexz�hler zur�cksetzen
	RxCtrl[idx].bReceiving   = 0;			//- SoT wurde empfangen -> Empfang einer Nachricht l�uft
 	RxCtrl[idx].bMsgComplete = 0;			//- 1 = Nachricht im Buffer ist vollst�ndig
}
//---------------------------------------------------------------------
/**
@brief		Der Funktion wird das empfangene Zeichen �bergeben.
			Das Zeichen wird dann in den Empfangspuffer eingef�gt, wenn:
			- der Zustand Empfang �berhaupt vorliegt
			- die max. L�nge der Nachricht noch nicht erreicht wurde

			MSG_LEN_MAX		L�nge der Payload = 15 Zeichen

@param		data enth�lt das empfangene Zeichen
			
@return		void

*/
void recSave( uint8_t data ) {
	if (RxCtrl[RxBufIdx].bReceiving) {							//wir sind noch auf Empfang	
	  if (RxCtrl[RxBufIdx].Index < MSG_LEN_MAX) {				//und haben noch Platz im Puffer
		RxBuf[RxBufIdx].Buf[ RxCtrl[RxBufIdx].Index ] = data;	//das empfangene Zeichen speichern
		RxCtrl[RxBufIdx].Index++;								//und den Z�hler auf die n�chste Position weiterschieben
		  if (RxCtrl[RxBufIdx].Index == MSG_LEN_MAX) {			//das letzte Byte ist erreicht
			  clearRxCtrl(RxBufIdx);							//- Buffer overrun;  Fehler: das letzte Zeichen muss ein ETX sein
		  }
	  } else {										//Kein Platz mehr im Empfangspuffer
		clearRxCtrl(RxBufIdx);						//- Buffer overrun		  
	  }
	} else {
		clearRxCtrl(RxBufIdx);						//- nicht empfangsbereit; kein STX erkannt
	}
	CLEAR_DLE_RX;		//Empfang des Maskierungszeichens im Status l�schen
}

//---------------------------------------------------------------------
/**
@brief		Liefert die L�nge der im Sendepuffer bereitstehenden Nachricht zur�ck.
@return		5				bei L�nge der Payload = 0
			..
			MSG_LEN_MAX		bei L�nge der Payload = 15 Zeichen
*/
uint8_t getTxMsgLen() {
	uint8_t x;
	x = TxBuf.Datagramm.mode.count;						//Anzahl der Bytes in der Payload
	return( x + MSG_HEADER_LEN + MSG_TRAILER_LEN );
}
//---------------------------------------------------------------------
/**
@brief	Liefert zur�ck, welches Zeichen einer Nachricht gesendet werden muss
		Anfang:		bFraming = 0   und   Index = 0;

		Daten:		bFraming = 1   und   Index = 0;			//das erste Datenbyte
						mit Index>0 und Index<MSG_HEADER_LEN 
						wird bFraming wieder zur�ckgesetzt					
					
		CS:			bFraming=0 und Index=getTxMsgLen()		//die cs wurde gesendet, jetzt noch das 

		Ende:		bFraming=1 und Index=getTxMsgLen()		//alles fertig
		
@return	 ST_DATA	0..MSG_LEN_MAX-1	Das Datenbype
		 ST_STX		das Zeichen 
		 ST_ETX		das Zeichen 
		 ST_DLE		das Zeichen wird von hier nicht selektiert
		 ST_CS		die Pr�fsumme soll gesendet werden
		 ST_NONE	keine Daten mehr zu �bertragen
*/
TSENDSTATE getSendState( ) {
  if  (TxCtrl.bMsgValid) {		//- Bei einer g�ltigen Nachricht...
	if (!TxCtrl.bFraming) {
		if (TxCtrl.Index == 0) {	
			TxCtrl.bFraming = 1;	//Das einleitende Zeichen des Frames senden
			return(ST_STX);
		} else {
			if (TxCtrl.Index < (getTxMsgLen()-MSG_TRAILER_LEN) ) {	//Senden l�uft noch im Bereich Header + Payload
				return( ST_DATA );
			} else {
			  if (TxCtrl.Index < (getTxMsgLen()) ) {		//Payload komplett durch
				return( ST_CS );
			  } else {
				TxCtrl.bFraming = 1;
				return( ST_ETX );
			  } 
			}
		}
	} else {
		if (TxCtrl.Index < MSG_HEADER_LEN) { 
			TxCtrl.bFraming = 0; 
			return( ST_DATA );
		} else {
			return( ST_NONE );
		}
	}
  } else {
	  return( ST_NONE );
  }
}
//---------------------------------------------------------------------
/**
@brief	Die Funktion �bertr�gt ein Byte in den Sendepuffer und erh�ht den Indexz�hler
		Sie pr�ft auch das evtl. Maskieren von Datenbytes mit DLE
		
@return	 keine
*/
void sendData( uint8_t data ) {
	if (IS_DLE_TX) {				//wenn bereits ein Maskierungszeichen gesendet wurde, dann die Daten auf jeden Fall �bertragen
		TxCtrl.Index++;
		UDR = data;		
		CLEAR_DLE_TX;
	} else {						//es wurde bisher noch kein Maskierungszeichen gesendet...
		if ( SEND_DLE(data) ) {		//es ist aber eines erforderlich
			UDR = FRAME_DLE;
			SET_DLE_TX;							
		} else {					//es ist auch keines erforderlich
			TxCtrl.Index++;
			UDR = data;
		}
	}
}

//---------------------------------------------------------------------
/**
@brief	UART Data Register Empty interrupt
		Die Funktion wird von d erHardware automatisch aufgerufen, wenn das Senderegister UDR leer ist
		also ein Byte vollst�ndig gesendet wurde.
		Die Funktion ermittelt daraufhin das n�chste zu sendende Byte und schreibt es in das Senderegister.
		�ber die Funktion sendData() wird das Byte-Stuffing, also das einf�gen eines evtl. erforderlichen
		Maskierungsbytes automatisch behandelt.
		Der Sendeprozess ist damit vollst�ndig Interrupt-getrieben und wird durch den Aufruf der Funktion
		sendMsg() gestartet.
		
@return	 keine
*/

ISR(USART_UDRE_vect){
	uint8_t data = 0;
	
	switch ( getSendState() ) {
		case ST_DATA:	data = TxBuf.Buf[ TxCtrl.Index ];
						sendData( data );	//Byte senden, ggf. DLE dazwischenschieben
						break;
		case ST_STX:	data = FRAME_STX;
						UDR = data;
						break;
		case ST_ETX:	data = FRAME_ETX;
						UDR = data;
						break;
		case ST_DLE:	data = FRAME_DLE;
						UDR = data;
						break;
		case ST_CS:		data = TxBuf.Datagramm.cs;
						sendData( data );	//Byte senden, ggf. DLE dazwischenschieben
						break;
		default: 		clearTxCtrl();					// ST_NONE	
						UCSRB &= ~(1 << UDRIE);			// Nichts mehr zu senden...			
	}
}
/*************************************************************************
Function: UART Tx Complete
Purpose:  Wird aufgerufen, wenn der Sendepuffer leer ist eg. nach dem 
          letzten Bit. Damit gut geeignet, die Sendestufe auszuschalten
**************************************************************************/
ISR(USART_TXC_vect) {
	SENDER_OFF;
}
//---------------------------------------------------------------------
/**
@brief	UART Receive Complete interrupt
		Diese Funktion (Interrupt-Handler) wird automatisch durch die Hardware aufgerufen,
		wenn ein komplettes Zeichen (8 Bit + Startschritt + Stoppschritt) �ber die 
		serielle Schnittstelle empfangen wurde.
		Der Empfang von Datagrammen ist vollst�ndig interrupt-gesteuert und l�uft somit im Hintergrund.
		
@param	keine		
		
@return	keine 
*/
ISR(USART_RXC_vect) {
  uint8_t  rxdata;
  
  rxdata = UDR;				//- read the byte
  
  if (!(TxCtrl.bSending)) {			//- wenn der Knoten nicht gerade selbst am senden ist
	switch (rxdata) {
	  case FRAME_STX:	if (IS_DLE_RX) {			//wurde ein Maskierungszeichen empfangen?
							recSave( rxdata );		//den maskierten Wert speichern
						} else {
							//Start of transmission;  Rahmenanfang erkannt, wird aber nicht gespeichert
							RxCtrl[RxBufIdx].Index        = 0;								//- starte index z�hler
							RxCtrl[RxBufIdx].bReceiving   = 1;								//- es wird gerade eine Nachricht empfangen
							RxCtrl[RxBufIdx].bOverrun     = RxCtrl[RxBufIdx].bMsgComplete;	// setze Overrun-Flag, wenn die letzte Nachricht noch nicht verarbeitet wurde
							RxCtrl[RxBufIdx].bMsgComplete = 0;								//- Nachricht noch nicht komplett
						}
						break;										
	  case FRAME_ETX:	if (IS_DLE_RX) {			//wurde ein Maskierungszeichen empfangen?
							recSave( rxdata );		//den maskierten Wert speichern
						} else {					//End of Transmission;	Rahmenende erkannt, wird aber nicht gespeichert
							if (RxCtrl[RxBufIdx].bReceiving) {								//wir sind noch auf Empfang
								RxCtrl[RxBufIdx].bReceiving   = 0;							//Empfang vollst�ndig beenden
								RxCtrl[RxBufIdx].bOverrun     = 0;							//kein �berlauf aufgetreten
								if (  (RxBuf[RxBufIdx].Datagramm.dst) == ownAdr() ) {		//ist diese Nachricht f�r mich?
									//- Check CRC											//Pr�fsumme validieren
									RxCtrl[RxBufIdx].bMsgComplete = 1;						//Nachricht ist ok
									RxBufIdx = (~RxBufIdx & 1);								//Empfang der n�chsten Nachricht in den anderen Puffer leiten
								} else {
									clearRxCtrl(RxBufIdx);			//Nachricht ist nicht f�r diesen Knoten, also weg damit
								}
							} else {
								clearRxCtrl(RxBufIdx);		//nicht empfangsbereit
							}
						}
						break;
	  case FRAME_DLE:	//Data Link Escape (Maskierungszeichen f�r transparente Payload)
						if (IS_DLE_RX) {			//wurde ein Maskierungszeichen empfangen?
							recSave( rxdata );		//den maskierten Wert speichern
						} else {
							SET_DLE_RX;				//Empfang des Maskierungszeichens im Status merken
						}
						break;
	  default:			recSave( rxdata );		//empfangenes Zeichen speichern, wenn m�glich
  	}	//end of switch
  }  //- evtl. eigene Nachricht, hier nicht weiter auswerten
}



/*----------------------------------------------------------------------------
  Initialisiert alles
------------------------------------------------------------------------------*/
void initSerial( void ) {
    uint8_t  sreg = SREG;
    uint16_t ubrr = (uint16_t) (((uint32_t)(F_CPU) + UART_BAUD_RATE * 8UL) / (UART_BAUD_RATE * 16UL) - 1UL);
					//Berechnung siehe -> http://www.mikrocontroller.net/articles/AVR-Tutorial:_UART#UART_konfigurieren


	//Steuerpin f�r Sendestufe auf Ausgabe schalten
	SENDER_DDR	|= ( 1<<SENDER_PIN );
	SENDER_OFF;

    //UBRRH = (uint8_t) (UART_BAUD_SELECT>>8);
    //UBRRL = (uint8_t) (UART_BAUD_SELECT);
	
    UBRRH = ( (uint8_t) (ubrr>>8)			//Das Bit URSEL muss gel�scht sein, damit der Schreibzugriff in UBRRH landet.	
	          & URSEL_MASK_WRITE_UBRR		// UBBRH und UCSRC liegen auf de rgleichen Adresse. Das Bit URSEL entscheidet, 
	        );								// wo ein Schreibzugriff landet
											// URSEL=0  -> UBRRH
											// URSEL=1	-> UCSRC
    UBRRL = (uint8_t) (ubrr);
    
    cli();									// Interrupts kurz deaktivieren 

	UCSRA &= ~( (1 << FE)  | 				//- Clear Framing Error
				(1 << DOR) |				//- Clear Data overrun error
				(1 << PE) );				//- Clear Parity error


    UCSRB |= (1 << RXEN)  | 				//- Receiver ein
	         (1 << TXEN)  | 				//- Transmitter ein
			 (1 << RXCIE) |					//- Receive Complete interrupt aktivieren
			 (1 << TXCIE);					//- Transmit Complete interrupt aktivieren
    
	
	UCSRC |= ( (1 << UCSZ1) |				//- Asynchronous USART, 8N1
			   (1 << UCSZ0) 
			 ) |  URSEL_MASK_WRITE_UCSRC;	//Das Bit URSEL muss gesetzt sein, damit der Schreibzugriff in UCSRC landet.	
    
     
    do {									// Flush Receive-Buffer (entfernen evtl. vorhandener ung�ltiger Werte)
        UDR;								// UDR auslesen (Wert wird nicht verwendet) 
    } while (UCSRA & (1 << RXC));
   
    UCSRA = ((1 << RXC) | (1 << TXC));		// R�cksetzen von Receive und Transmit Complete-Flags 
	
    SREG = sreg;					    	// Global Interrupt-Flag wieder herstellen 
    clearTxCtrl();
	dle_status = 0;
}
//---------------------------------------------------------------------
/**
@brief	Startet die eigentliche �bertragung, nachdem durch einen Aufruf der Funktion sendMsg()
		eine Nachricht �bergeben und die Sendestufe eingeschaltet wurde.
		Startet das Senden, indem ein TransmitBufferEmpty-IRQ ausgel�st wird.
		Die Funktion kehrt sofort zur�ck. Der eigentliche Versand des Datagrammes erfolgt im Hintergrund.
		
		Interne Funktion.
		
@param	keine	
@return	keine
*/
void startTransmit( void ) {
  UCSRB |= (1 << UDRIE);	//- 
}

//---------------------------------------------------------------------
/**
@brief	�bergibt die in msg enthaltene Nachricht an die Sendestufe und veranlasst deren �bertragung.
		Die funktion kehrt sofort zur�ck. Der eigentliche Versand des Datagrammes erfolgt im Hintergrund.
		
@param	msg	
		Die zu versendende Nachricht. 
		
@return	keine
*/
void sendMsg( TDataBuf msg ) {
  if (!(TxCtrl.bMsgValid)) {	//Msg nur �bernehmen, wenn Sendepuffer frei ist
    TxBuf = msg;
	TxCtrl.bMsgValid = 1;		//Sendepuffer jetzt belegt
	SENDER_ON;					//enable sender
	TxCtrl.bSending = 1;
	_delay_us(10);				//einen kurzen Moment warten, bis die Sendestufe eingeschaltet ist (Einschwingzeit evtl. nicht n�tig?)
	startTransmit();
  }
}
//---------------------------------------------------------------------
/**
@brief	Pr�ft, ob eine Nachricht empfangen wurde und liefert diese zur Weiterverarbeitung zur�ck.
		Diese Funktion muss regelm�ssig aufgerufen werden, damit keine Datagramme verlorengehen.
		
@param	msg	
		Ein pointer auf eine Struktur TDataBuf. In diese Struktur wird die empfangene Nachricht kopiert,
		wenn es eine g�ltige Nachricht gibt. Im anderen Fall bleibt der Puffer msg unver�ndert.			
		
@return	0 = keine Nachricht,  msg wird nicht ver�ndert.
		1 = g�ltige Nachricht, Nachricht steht in msg 
*/
uint8_t peekMsg( TDataBuf *msg ) {
  uint8_t result;
  uint8_t idx;
  
  cli();
  idx = (~RxBufIdx & 1);			//- immer nur den Puffer abfragen, der gerade nicht als Empfangspuffer verwendet wird
  if (RxCtrl[idx].bMsgComplete) {
  	result = 1;
  	(*msg) = RxBuf[idx];
	RxCtrl[idx].bMsgComplete = 0;
	RxCtrl[idx].bOverrun = 0;
  } else {
  	result = 0;
  } 
  sei();
  return (result);
}



