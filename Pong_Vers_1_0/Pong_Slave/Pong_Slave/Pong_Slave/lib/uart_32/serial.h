/*  Datentypen zu serial

*/
#ifndef _SERIAL_
#define _SERIAL_

#include <avr/io.h>
#include "protocol.h"


#define SENDER_DDR		DDRB
#define SENDER_PORT		PORTB
#define SENDER_PIN		4

#define SENDER_OFF		(SENDER_PORT &= ~(1<<SENDER_PIN))	//- Portpin auf 0 -> DE aus
#define SENDER_ON		(SENDER_PORT |=  (1<<SENDER_PIN))	//- Portpin auf 1 -> DE ein

// Schicht 2 Informationen
//  
#define FRAME_STX		0x02		//Start of Transmission
#define FRAME_ETX		0x03		//End of Transmission
#define FRAME_DLE		0x1F		//Data Link Escape (Maskierungszeichen f�r transparente Payload)
#define FRAME_SIZE		0x02		//2 Byte


typedef struct {
   unsigned bSending:1;  			// 1 = Sendestufe aktiv
   unsigned bMsgValid:1; 			// 1 = G�ltige Nachricht zu versenden
   unsigned bFraming:1;    			// 1 = das entsprechende Zeichen f�r das Framing wurde gesendet	
   unsigned Index:5;     			// 0..31 Z�hler indexiert die Bytes einer Nachricht
} TSendCtrl;

typedef struct {
   unsigned bReceiving:1;  			// 1 = Startzeichen wurde gefunden
   unsigned bMsgComplete:1; 		// 1 = Empfangene Nachricht ist vollst�ndig (es wurden 16 Zeichen gelesen)
   unsigned bOverrun:1;				// 1 = received message was not read bevor this buffer is used for receiving
   unsigned Index:5;     			// 0..31 Z�hler indexiert die Bytes einer Nachricht
} TRecCtrl;


#define UART_BAUD_RATE      		9600UL   		//- Definition als unsigned long, sonst gibt es Fehler in der Berechnung   
#define UART_BAUD_SELECT 			( (F_CPU + UART_BAUD_RATE*8UL) / (UART_BAUD_RATE*16UL) -1 )		//-> http://www.mikrocontroller.net/articles/AVR-Tutorial:_UART#UART_konfigurieren

#define URSEL_MASK_WRITE_UCSRC		(1<<URSEL)		//das Bit muss beim Schreiben auf das Register UCSRC = 1 sein 
#define URSEL_MASK_WRITE_UBRR		(~(1<<URSEL))	//das Bit muus beim Schreiben auf das Register UBRRH = 0 sein
													//siehe Datenblatt
typedef enum {	ST_DATA,
				ST_STX,
				ST_ETX,
				ST_DLE,
				ST_CS,
				ST_NONE
			 } TSENDSTATE; 


#define	DLE_TRANSMITTED				1		//Flag f�r DLE gesendet
#define DLE_RECEIVED				2		//Flag f�r DLE empfangen



/*---------------------------------------------------------------- 
  Prototypes
----------------------------------------------------------------*/
void    initSerial( void );
void    sendMsg( TDataBuf msg );
uint8_t peekMsg( TDataBuf *msg );




#endif	//_SERIAL_
