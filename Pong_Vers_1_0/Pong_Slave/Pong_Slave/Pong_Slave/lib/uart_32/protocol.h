/*  datatypes f�r elektor bus protocol

*/
#ifndef _PROTOCOL_
#define _PROTOCOL_


//#include <inttypes.h>

/*  Sobald Computersysteme verschiedener Architekturen miteinander kommunizieren, 
    muss man sich Gedanken um die Reihenfolge von Bytes bei Datentypen machen, die aus mehreren Bytes bestehen.
	Ein 32-Bit-Wert (z.B. Integer) besteht aus 4 Byte.
	
	Man k�nnte zuerst das h�chstwertigste Byte �bertragen und das niederwertigste zuletzt  (-> Big Endian)
	oder genau umgekehrt (-> Little endian)
	
	Hier muss also eine vereinbarung her.
	
	Weiterf�hrende Informationen hier zu siehe https://de.wikipedia.org/wiki/Byte-Reihenfolge
*/

#define LITTLE_ENDIAN

// Endian converters 
#define Le16(b)                             \
   (  ((uint16_t)(     (b) &   0xFF) << 8)  \
   |  (     ((uint16_t)(b) & 0xFF00) >> 8)  \
   )
#define Le32(b)                             \
   (  ((uint32_t)(			(b) &      0xFF)  << 24)  \
   |  ((uint32_t)((uint16_t)(b) &     0xFF00) <<  8)  \
   |  (     (	  (uint32_t)(b) &   0xFF0000) >>  8)  \
   |  (     (	  (uint32_t)(b) & 0xFF000000) >> 24)  \
   )

// host to network conversion: used for Intel HEX format, TCP/IP, ... 
// Convert a 16-bit value from host-byte order to network-byte order 
// Standard Unix, POSIX 1003.1g (draft) 
// see:  http://www.intel.com/design/intarch/papers/endian.pdf
//
#ifdef LITTLE_ENDIAN 
#  define htons(a)    Le16(a) 
#define ntohs(a)    htons(a) 
#  define htonl(a)    Le32(a) 
#define ntohl(a)    htonl(a) 
#else 
#define htons(a)    (a) 
#define ntohs(a)    (a) 
#define htonl(a)    (a) 
#define ntohl(a)    (a) 
#endif 




#define MSG_HEADER_LEN		4				// 4 Byte ist der Header lang:  SRC, DST, MT, CNT
#define MSG_PAYLOAD_LEN     16				// max. Anzahl von Bytes in der Payload
#define MSG_TRAILER_LEN		1				// f�r die Pr�fsumme

#define MSG_LEN_MAX			MSG_HEADER_LEN + MSG_PAYLOAD_LEN + MSG_TRAILER_LEN		//max. Anzahl von Bytes pro Nachricht

#define NO_CHECKSUM			0
#define WITH_CHECKSUM		1
#define NO_ACK_REQUIRED		0
#define ACK_REQUIRED		1


//- CNT Byte definition	//- starts with LSB
typedef struct {
	unsigned count:4;				// Anzahl der Bytes in der Payload  0..15
	unsigned bCRC:1;				// 0=no checksum							1=with checkcsum
	unsigned bAckReq:1;				// 0=Acknowledge message is not required	1=is required
	unsigned bRes0:1;				// Reserve
	unsigned bRes1:1;				// Reserve
} TMode;


typedef uint8_t TBuf[MSG_LEN_MAX];				// Puffer f�r eine Nachricht ohne STX / ETX  aber mit CS
typedef uint8_t TPayload[MSG_PAYLOAD_LEN];		// Puffer f�r die Payload 


//- Message Level 
//- die Framingbytes sind hier nicht mehr enthalten
//- 
typedef  struct {							// Aufbau einer Nachricht
  uint8_t   src;							// Quelladresse	(Absender)
  uint8_t   dst;							// Zieladresse		(Empf�nger)
  uint8_t	mt;								// Message Type
  TMode     mode;							// Mode byte, describing the structure of the message
  TPayload  Payload;						// usage depending on application level
  uint8_t	cs;								// Pr�fsumme, z.B. crc-8
                } TDatagramm;

typedef union  {
      TBuf        Buf;						//- Zugriff als Array of Byte m�glich
	  TDatagramm  Datagramm;				//- Zugriff �ber Struktur m�glich
               } TDataBuf;

//- Application Level -
// tbd
		
		
// Nachrichtentypen

#define MT_STATUS_REQ		0x10
#define MT_STATUS_RESP		MT_STATUS_REQ + 1

#define MT_DISPLAY_REQ		0x12
#define MT_DISPLAY_RESP		MT_DISPLAY_REQ + 1

#define MT_KEY_REQ			0x14
#define MT_KEY_RESP			MT_DISPLAY_REQ + 1

/*---------------------------------------------------------------- 
  Prototypes f�r die Adressierung
----------------------------------------------------------------*/
uint8_t ownAdr();
uint8_t masterAdr();


/*---------------------------------------------------------------- 
  Prototypen auf der Ebene der Datagramme
----------------------------------------------------------------*/
void msgClear( TDataBuf *msg );
void msgPrepare( TDataBuf *msg );






#endif	//_PROTOCOL_
