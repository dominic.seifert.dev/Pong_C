/*

 * crc8.c
 *
 * Created: 16.10.2015 18:56:09
 *  Author: busser.michael
 */ 

#include <avr/io.h>
#include "crc8.h"

uint8_t reg=0;	// Rechen-Register fuer den CRC Wert mit Initial Value 0

uint8_t crc8_bytecalc(uint8_t databyte) {
	
	uint8_t i;				// Schleifenzaehler
	uint8_t flag;			// flag um das oberste Bit zu merken
	uint8_t polynom = 0xd5;	// Generatorpolynom

	// gehe fuer jedes Bit der Nachricht durch
	for(i=0; i<8; i++) {
		if(reg&0x80) flag=1; else flag=0;	// Teste MSB des Registers
		reg <<= 1;							// Schiebe Register 1 Bit nach Links und
		if(databyte&0x80) reg|=1;				// F�lle das LSB mit dem naechsten Bit der Nachricht auf
		databyte <<= 1;							// n�chstes Bit der Nachricht
		if(flag) reg ^= polynom;			// falls flag==1, dann XOR mit Polynom
	}
	return reg;
}


/* crc8_messagecalc

	Ruft crc8_bytecalc solange auf bis die Nachricht vollstaendig abgearbeitet wurde, danach
	wird crc8_bytecalc noch einmal mit Wert 0 aufgerufen um die CRC-8 Berechnung abzuschliessen

	Parameter:
	msg ... Bytearray welches die Nachricht enthaelt
	len ... Laenge der Nachricht
	Return: CRC-8 der kompletten Nachricht
*/

uint8_t crc8_messagecalc(uint8_t *msg, uint8_t len) {
	uint8_t i;
	for(i=0; i<len; i++) {
		crc8_bytecalc(msg[i]);			// Berechne fuer jeweils 8 Bit der Nachricht
	}
	return crc8_bytecalc(0);			// die Berechnung muss um die Bitlaenge des Polynoms mit 0-Wert fortgefuehrt werden
}

