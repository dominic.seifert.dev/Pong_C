/*
 * key.h
 *
 * Created: 04.08.2014 11:49:42
 * Author: busser.michael
 *
 * Die Implementierung geht auf einen Vorschlag und ein Beispiel von Peter Dannegger zur�ck
 * siehe: http://www.mikrocontroller.net/articles/Entprellung,  Komfortroutine (C f�r AVR)  
 *
 * Es k�nnen max. 8 Tasten gleichzeitg verarbeitet werden. 
 * Die Methode erwartet Tasterkontakte, die active-Low sind, d.h. im Ruhezustand also also eine 1 liefern 
 *
 * Tutorial zum Entprellen von Tasten:    http://www.mikrocontroller.net/articles/AVR-Tutorial:_Tasten
 * Verschiedene Methoden zum entprellen:  http://www.mikrocontroller.net/articles/Entprellung#Timer-Verfahren_.28nach_Peter_Dannegger.29
 * Dieser Thread beschreibt noch einmal, 
 * wie man diesehr trickreichen Codeteile 
 * von Peter Dannegger in eigene 
 * Programm einbindet.                    http://www.mikrocontroller.net/topic/98195
 *
 *
 * Normalerweise sollte man keinen Code verwenden, den man nicht versteht.
 * Dieses Beispiel von Peter Dannegger ist eine (seltene) Ausnahme von dieser Regel.
 *
 */ 


#ifndef KEY_H_
#define KEY_H_

#include <stdint.h>				//Definition der Datentypen einbinden
#include <avr/io.h>	
#include <avr/interrupt.h>		//wg. sei und cli

#define KEY_DDR         DDRA
#define KEY_PORT        PORTA
#define KEY_PIN         PINA
#define KEY0            5
#define KEY1            4
#define KEY2            3
#define KEY3			2
#define KEYROT			1
#define ALL_KEYS        (1<<KEY0 | 1<<KEY1 | 1<<KEY2 | 1<<KEY3 | 1<<KEYROT)
#define KEY_PULLUP		(1<<KEY0 | 1<<KEY1 | 1<<KEY2 | 1<<KEY3)

#define REPEAT_MASK     (1<<KEY1 | 1<<KEY2)       // repeat: key1, key2
#define REPEAT_START    50                        // after 500ms
#define REPEAT_NEXT     20                        // every 200ms

#define TASTE_GELB		(1<<KEY0)
#define TASTE_ROT		(1<<KEY1)
#define TASTE_BLAU		(1<<KEY2)
#define TASTE_GRUEN		(1<<KEY3)
#define TASTE_ROTARY	(1<<KEYROT)

//--- Funktionsprototypen ----
void 	getKeyState( void );					//regelm�ssig im Intervall von 10ms aufrufen
void	key_init();				
uint8_t get_key_press( uint8_t key_mask );		//
uint8_t get_key_rpt( uint8_t key_mask );		//
uint8_t get_key_state( uint8_t key_mask );		//
uint8_t get_key_short( uint8_t key_mask );		//
uint8_t get_key_long( uint8_t key_mask );		//





#endif /* KEY_H_ */