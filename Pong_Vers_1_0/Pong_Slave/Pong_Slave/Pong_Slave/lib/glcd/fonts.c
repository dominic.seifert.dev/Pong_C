/*
	Eine Bibliothek mit Grafikfunktionen f�r grafikf�hige Displays
	Hier sind die Funktionen im Zusammenhang mit Zeichens�tzen enthalten


*/

#include <avr/io.h>
#include <inttypes.h>
#include "fonts.h"


/*------------------------------------------------------------------------------
   Helper Functions to find, retrieve and display characters
   found at http://www.mikrocontroller.net
------------------------------------------------------------------------------*/

//load pointer to data table
inline PGM_P font_data(FONT_P charset) {
  PGM_P tmp;
  if (sizeof(tmp) == 2)
    tmp = (PGM_P)pgm_read_word(&(charset->data));
  else
    memcpy_P((char*)&tmp,&(charset->data),sizeof(tmp));
  return tmp;
  }



//load pointer to width table
inline PGM_P font_widthtable(FONT_P charset) {
  PGM_P tmp;
  if (sizeof(tmp) == 2)
    tmp = (PGM_P)pgm_read_word(&(charset->widthtable));
  else
    memcpy_P((char*)&tmp,&(charset->widthtable),sizeof(tmp));
  return tmp;
  }
  


//load height of font in pixels
inline uint8_t font_get_height_pixels(FONT_P charset) {
  uint8_t t = pgm_read_byte(&charset->height);
  return ( t );
  }

//load height of font in bytes
inline uint8_t font_get_height_bytes(FONT_P charset) {
  uint8_t t = pgm_read_byte(&charset->height);
  return ((t-1)/8+1);
  }



//is additional space needed to the right of the character?
inline uint8_t font_get_add_space(FONT_P charset, unsigned char character) {
  PGM_P type = font_widthtable(charset);
  if ( type != 0 ) //if there is a width table, then it's a proportional font
    return 1;
  else
    return 0;
  }



//get character number in charset
inline int16_t font_get_char_number(FONT_P charset, unsigned char character) {
  uint8_t first = pgm_read_byte(&charset->firstchar);
  uint8_t last  = first + pgm_read_byte(&charset->charcount);
  if (character < first || character > last) return -1;
  else return character - first;
  }



//get width of character
inline uint8_t font_get_char_width(FONT_P charset, unsigned char character) {
  PGM_P table = font_widthtable(charset);
  if (table)
    return pgm_read_byte(table+font_get_char_number(charset,character));
  else
    return pgm_read_byte(&charset->width);
  }



//find position of character in flash
PGM_P font_get_char_position(FONT_P charset, unsigned char character) {
  uint16_t ret         = 0;
  int16_t  charnum_ret = font_get_char_number(charset, character);
  uint8_t  charnum     = charnum_ret;
  PGM_P    base        = font_widthtable(charset);

  if (charnum_ret < 0)          //char not found
    return 0;
  if(base == 0)            //fixed width
    return font_data(charset) + (uint16_t)charnum * (uint8_t)(font_get_height_bytes(charset) * font_get_char_width(charset,character));
  if (charnum)             //proportional width
    while(charnum--)
      ret += pgm_read_byte(base++);
  return (font_data(charset))+ret*font_get_height_bytes(charset);
  }


/*------------------------------------------------------------------------------
  Puts character c of font charset to the display buffer 
  upper left corner ist at (x,y)
  returns the width of the char
------------------------------------------------------------------------------*/
uint8_t fontPutChar(FONT_P charset, unsigned char character, uint8_t x, uint8_t y) {
 static uint8_t  i, j, row, col, pix, k;

  //load information about character from font
  uint8_t char_width    = font_get_char_width(charset,character); 
  uint8_t font_height   = font_get_height_bytes(charset);
  uint8_t font_pix      = font_get_height_pixels(charset); 
  uint8_t free_space    = font_get_add_space(charset,character);
  PGM_P   tableposition = font_get_char_position(charset,character);


  for (row=0; row<font_height; row++) {
    pix = font_pix - ( row << 3 );             //- nr of pixels left for the following row
	pix = (pix > 8)?8:pix;					   //- let pix not become bigger than 8	
	for (col=0; col<char_width; col++ ) {
	  i = pgm_read_byte( tableposition + row*char_width + col );  
      for (j=0; j<pix; j++) {
	    k = i & (1 << j);
		lcd_Pixel( x+col, y+(row<<3)+j,   k  );
	  }
	}
  }
  return(char_width + free_space);
}

/*------------------------------------------------------------------------------
   Puts a string from RAM or Flash to the display buffer
------------------------------------------------------------------------------*/
void fontPutStringP(FONT_P font, PGM_P str, uint8_t x, uint8_t y ) {
  unsigned char t;
  uint8_t  offset = 0;
  while((t = pgm_read_byte(str++)))  {
    if (t != 32) { 
	  offset += fontPutChar(font, t, x+offset, y ); 
	} else {
	  offset += 3;	
	}
  }
}


void fontPutString(FONT_P font, char* str, uint8_t x, uint8_t y ) {
  unsigned char t;
  uint8_t  offset = 0;
  while((t = *str++)) {
    if (t!= 32) { 
      offset += fontPutChar(font,t, x+offset, y );
    } else {
      offset +=3;
	}
  }
}




