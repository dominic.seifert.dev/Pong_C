/*  

Headerfile f�r die Bibliothek mit Grafikroutinen


*/

#ifndef GLCD
#define GLCD



#include "glcd_driver.h"		//- Include display driver
#include "fonts.h"



/*------------------------------------------------------------------------------
 Funktionsprototypen
 der Routinen, die nach au�en sichtbar sind.
------------------------------------------------------------------------------*/
void gInit( void );								//- initializing display
void gClear(void);								//- Clear Display RAM
void gDrawPixel(uint8_t x, uint8_t y);			//- Draws a pixel at x,y in display buffer, no transfer to display
void gClearPixel(uint8_t x, uint8_t y);			//- Clears a pixel at x,y in display buffer, no transfer to display
void gFlush( void );							//- transfers display buffer to display

void gDrawLine( uint8_t x0, uint8_t y0, uint8_t x1, uint8_t y1 );
void gClearLine( uint8_t x0, uint8_t y0, uint8_t x1, uint8_t y1 );
void gDrawRect(uint8_t x,uint8_t y, uint8_t width,uint8_t height, uint8_t fill );
void gClearRect(uint8_t x,uint8_t y,uint8_t width,uint8_t height );
void gDrawCircle(uint8_t x1, uint8_t y1, uint8_t radius, uint8_t fill );
void gClearCircle(uint8_t x1, uint8_t y1, uint8_t radius );





#endif //GLCD
