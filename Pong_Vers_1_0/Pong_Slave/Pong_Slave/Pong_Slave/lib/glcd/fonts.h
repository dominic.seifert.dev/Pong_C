/*  

    Definitionen der Datenstruktur eines Zeichensatzes

*/

#ifndef _FONTS_
#define _FONTS_

#include <avr/io.h>
#include <avr/pgmspace.h>
#include "glcd_driver.h"

/*------------------------------------------------------------------------------
  Select which of the available fonts will be included
  If a font is not needed, then you don't need to put it to the Flash 
  Just comment one of the defines below
------------------------------------------------------------------------------*/

#define FONT_INCLUDE_Times10
#define FONT_INCLUDE_Times16
#define FONT_INCLUDE_Times24



struct font_info {
    uint16_t size;       //size of data array
    uint8_t  width;      //(maximum) width of character
    uint8_t  height;     //height of character
    uint8_t  firstchar;  //the number of the first included character (often 0x20)
    uint8_t  charcount;  //nr of chars includes in the font, beginning with firstchar
    PGM_P    widthtable; //Pointer to the table holding character widths (NULL for monospaced fonts)
    PGM_P    data;       //Pointer to data arrray
    };


typedef const struct font_info PROGMEM* FONT_P;



/*------------------------------------------------------------------------------
  All font structures + verbose name defines
------------------------------------------------------------------------------*/
#ifdef FONT_INCLUDE_Times10
  extern const struct font_info fontTimes10 PROGMEM;
  #define FONT_TIMES_10 &fontTimes10
#endif

#ifdef FONT_INCLUDE_Times16
  extern const struct font_info fontTimes16 PROGMEM;
  #define FONT_TIMES_16 &fontTimes16
#endif

#ifdef FONT_INCLUDE_Times24
  extern const struct font_info fontTimes24 PROGMEM;
  #define FONT_TIMES_24 &fontTimes24
#endif







/*------------------------------------------------------------------------------
 Funktionsprototypen
------------------------------------------------------------------------------*/
void fontPutStringP(FONT_P font, PGM_P str, uint8_t x, uint8_t y );
void fontPutString(FONT_P font, char* str, uint8_t x, uint8_t y );
uint8_t fontPutChar(FONT_P charset, unsigned char character, uint8_t x, uint8_t y);




#endif //_FONTS_
