/*
	Eine Bibliothek mit Grafikfunktionen f�r grafikf�hige Displays
	Hier sind die Zeichenfunktionen enthalten, welche der Benutzer verwendet.

	Die Routinen hier benutzen die Funktionen, welche der Treiber glcd_driver bereitstellt
	Der Benutzer verwendet keine lcd_* Funktionen.

*/


#include <avr/io.h>
#include <inttypes.h>
#include <util/delay.h>
#include "glcd.h"




/*------------------------------------------------------------------------------
    
	Help functions NOT ment for API use

------------------------------------------------------------------------------*/
uint8_t hAbsDiff(uint8_t a,uint8_t b){ 
  if(a>b) return a-b;  else return b-a;
}

//------------------------------------------------------------------
//- General line operation from x to y using  Bresenham's line algorithm
//- ref: http://en.wikipedia.org/wiki/Bresenham%27s_line_algorithm
//- 
//-  Source:  Sami Varjo 
//-  Work File            : lcd_graphics.c
//- 
void hLine(uint8_t x0, uint8_t y0, uint8_t x1, uint8_t y1, uint8_t pix) {
  
  uint8_t dx, dy, i,j, _x1, _y1, _x0, _y0, is_steep;
  int8_t error, ystep;

  is_steep = hAbsDiff(y0,y1) > hAbsDiff(x0,x1);  

  //order the init points ascending
  if ( is_steep ){_x0=y0; _y0=x0; _x1=y1; _y1=x1; }
  else {_x0=x0; _x1=x1; _y0=y0; _y1=y1;  }
  if (_x0 > _x1) {i=_x0; _x0=_x1; _x1=i; i=_y0; _y0=_y1; _y1=i;}

  //check boundaries
  if (_x0>=lcd_X) _x0=0;
  if (_x1>=lcd_X) _x1=0;
  if (_y0>=lcd_X) _y0=0;
  if (_y1>=lcd_X) _y1=0;
  
  dx= _x1 - _x0;
  dy=hAbsDiff(_y0,_y1);  
  error = -(dx+1)/2;
  j=_y0;
  if (_y0<_y1) ystep=1;  else ystep=-1;

  for(i=_x0;i<=_x1;i++){
    if (is_steep) lcd_Pixel(j,i, pix);
    else lcd_Pixel(i,j, pix);
    error += dy;
    if (error>0){
      j=j+ystep;
      error=error-dx;
    }
  }
}

//------------------------------------------------------------------------------
//- General circle operation (!NON OPTIMIZED = same pixels drawn multiple times)
//- 
//-  Source:  Sami Varjo 
//-  Work File            : lcd_graphics.c
//- 
void hCirc(uint8_t x1, uint8_t y1, uint8_t radius, uint8_t fill, uint8_t pix ) {  

  uint8_t  y=0, x=0, d = 0;
  int8_t part; 
  d = y1 - x1;
  y = radius;
  part = 3 - 2 * radius;
  if (fill) {				//- a filled circle
  	while (x <= y) {
	    hLine(x1 + x, y1 + y,x1 + x, y1 - y, pix);
    	hLine(x1 - x, y1 + y,x1 - x, y1 - y, pix);
    	hLine((y1 + y - d), y1 + x,(y1 + y - d), y1 - x, pix);
    	hLine((y1 - y - d), y1 + x,(y1 - y - d), y1 - x, pix);
	    if (part < 0) part += (4 * x + 6);
    		else {
		      part += (4 * (x - y) + 10);
		      y--;
    		}
    	x++;
  	}
  } else {					//- only the circle line, not filled
	while (x <= y) {

		lcd_Pixel( x1 + x, y1 + y, pix ); 		lcd_Pixel( x1 + x, y1 - y, pix ); 
		lcd_Pixel( x1 - x, y1 + y, pix ); 		lcd_Pixel( x1 - x, y1 - y, pix ); 
		lcd_Pixel( (y1 + y - d), y1 + x, pix );	lcd_Pixel( (y1 + y - d), y1 - x, pix );
		lcd_Pixel( (y1 - y - d), y1 + x, pix );	lcd_Pixel( (y1 - y - d), y1 - x, pix );
	    if (part < 0) part += (4 * x + 6);
	    else {
    	  	part += (4 * (x - y) + 10);
	    	y--;
    	}
    	x++;
  	}
  }
}
//------------------------------------------------------------------
//- Schreibt das Byte data als Spalte in den display buffer
//- das LSB landet an der Position x,y, das n�chste Bit an x,y+1 usw.
//-
void hDataColumn(int8_t x, uint8_t y, uint8_t data, uint8_t transparent ){
  uint8_t row;
  for (row=0; row<8; row++,y++)
  {
	if (transparent) {
		if ((data & (1<<row)) != 0)
			lcd_SetPixel(x, y);
		else
			lcd_ClearPixel(x, y);

	} else {
		if ((data & (1<<row)) != 0)
			lcd_SetPixel(x, y);
	}
  }
}

//------------------------------------------------------------------
//------------------------------------------------------------------


/*------------------------------------------------------------------

  Exported functions of the lib.

------------------------------------------------------------------*/


//------------------------------------------------------------------
void gInit( void ) {
	lcd_Reset();
	lcd_Init( cDT_DOGM128, 0 );
}
//------------------------------------------------------------------
//- Clear Display RAM
void gClear(void) {	
	lcd_Clear();
}
//------------------------------------------------------------------
//- Set a pixel at x,y in display buffer, no transfer to display
void gDrawPixel(uint8_t x, uint8_t y) {			
	lcd_SetPixel( x, y );
}
//------------------------------------------------------------------
//- Clears a pixel at x,y in display buffer, no transfer to display
void gClearPixel(uint8_t x, uint8_t y) {			
	lcd_ClearPixel( x, y );
}
//------------------------------------------------------------------
//- transfers display buffer to display
void gFlush( void ) {							
	lcd_Flush();
}
//------------------------------------------------------------------
void gDrawLine( uint8_t x0, uint8_t y0, uint8_t x1, uint8_t y1 ) {
	hLine( x0, y0, x1, y1, 1 );
}
//------------------------------------------------------------------
void gClearLine( uint8_t x0, uint8_t y0, uint8_t x1, uint8_t y1 ) {
	hLine( x0, y0, x1, y1, 1 );
}
//------------------------------------------------------------------
//- x,y    = left upper corner
//- width  = width in pixel
//- height = height in pixel
void gDrawRect(uint8_t x,uint8_t y,uint8_t width,uint8_t height, uint8_t fill ) {

  if (fill) {
	uint8_t  i;
	for (i=y; i<y+height; i++) {  hLine( x, i, x+width-1, i, 1 ); };

  } else {
	gDrawLine(x,        y,          x+width-1, y);
	gDrawLine(x,        y,          x,         y+height-1);
	gDrawLine(x+width-1,y+height-1, x+width-1, y);
	gDrawLine(x,        y+height-1, x+width-1, y+height-1);  
  }
}
//------------------------------------------------------------------
void gClearRect(uint8_t x,uint8_t y,uint8_t width,uint8_t height ) {
	uint8_t  i;
	for (i=y; i<y+height; i++) {  hLine( x, i, x+width-1, i, 0 ); };
}
//------------------------------------------------------------------
void gDrawCircle(uint8_t x1, uint8_t y1, uint8_t radius, uint8_t fill ) {  
	hCirc( x1, y1, radius, fill, 1 );
}
//------------------------------------------------------------------
void gClearCircle(uint8_t x1, uint8_t y1, uint8_t radius ) {  
	hCirc( x1, y1, radius, 1, 0 );
}

//------------------------------------------------------------------
//------------------------------------------------------------------

















