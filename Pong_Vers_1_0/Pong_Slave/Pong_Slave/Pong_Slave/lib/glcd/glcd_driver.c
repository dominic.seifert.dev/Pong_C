/*
  Eine Bibliothek mit Grafikfunktionen f�r grafikf�hige Displays
  mit den folgenden Controllern / Displaytypen:
    - ST7565  DOG-M 128x64
    - ST7565  DOG-M 132x64

*/


#include <avr/io.h>
#include <inttypes.h>
#include <util/delay.h>
#include "glcd_driver.h"


//-----------------------------------------------------------------------------------------
void setDispRAM( uint16_t index, uint8_t value ) {
  uint16_t s;
  s = RAM_SIZE;
  if ((index >= 0) && ( index < s )) {
	disp_ram[ index ] = value;
  }
}

//-----------------------------------------------------------------------------------------
void _SPI_out(char out)
{
  char msk;

  msk = 0x80;
  do
   { ClrBit(LCD_SCL_PORT, LCD_SCL);
     if(out & msk)
        SetBit(LCD_SI_PORT, LCD_SI);
     else
        ClrBit(LCD_SI_PORT, LCD_SI);
   	SetBit(LCD_SCL_PORT, LCD_SCL);
   	msk >>= 1;
   }
  while(msk > 0);
}
//-----------------------------------------------------------------------------------------
void initDOGM132( uint8_t topview )
{
  lcd_Contr = 33;
  lcd_X     = 132;
  lcd_Y		= 64;
  lcd_Pages = 4;
  g_inverted= 0; 

  LCD_ENABLE;
  LCD_COMMAND;

  _SPI_out(CC_DISPSTART + 0);
  if(topview)
   { lcd_X_offs = 0;
     _SPI_out(CC_ADCNORMAL);
     _SPI_out(CC_COMREVERSE);
   }
  else
   { lcd_X_offs = 0;
     _SPI_out(CC_ADCREVERSE);
     _SPI_out(CC_COMNORMAL);
   }
  _SPI_out(CC_DISPNORMAL);
  _SPI_out(CC_LCDBIAS9);
  _SPI_out(CC_SETPOWERCTRL+7);
  _SPI_out(CC_BOOSTERRATIO);     _SPI_out(0);
  _SPI_out(CC_REGRESISTOR+3);
  _SPI_out(CC_SETCONTRAST);      _SPI_out(lcd_Contr);
  _SPI_out(CC_STATINDMODE);      _SPI_out(0);
  _SPI_out(CC_DISPON);

  LCD_DISABLE;
}

//-----------------------------------------------------------------------------------------
void initDOGM128( uint8_t topview )
{
  lcd_Contr = 22;
  lcd_X     = 128;
  lcd_Y		= 64;
  lcd_Pages = 8;
  g_inverted= 0; 

  LCD_ENABLE;
  LCD_COMMAND;

  _SPI_out(CC_DISPSTART + 0);
  if (topview)
   { lcd_X_offs = 4;
     _SPI_out(CC_ADCNORMAL);
     _SPI_out(CC_COMREVERSE);
   }
  else
   { lcd_X_offs = 0;
     _SPI_out(CC_ADCREVERSE);
     _SPI_out(CC_COMNORMAL);
   }
  _SPI_out(CC_DISPNORMAL);
  _SPI_out(CC_LCDBIAS9);
  _SPI_out(CC_SETPOWERCTRL+7);
  _SPI_out(CC_BOOSTERRATIO);     _SPI_out(0);
  _SPI_out(CC_REGRESISTOR+7);
  _SPI_out(CC_SETCONTRAST);      _SPI_out(lcd_Contr);
  _SPI_out(CC_STATINDMODE);      _SPI_out(0);
  _SPI_out(CC_DISPON);

  LCD_DISABLE;
}

//-----------------------------------------------------------------------------------------
void initDOGL128( uint8_t topview)
{
  lcd_Contr = 16;
  lcd_X     = 128;
  lcd_Y		= 64;
  lcd_Pages = 8;
  g_inverted= 0; 

  LCD_ENABLE;
  LCD_COMMAND;

  _SPI_out(CC_DISPSTART + 0);
  if(topview)
   { lcd_X_offs = 4;
     _SPI_out(CC_ADCNORMAL);
     _SPI_out(CC_COMREVERSE);
   }
  else
   { lcd_X_offs = 0;
     _SPI_out(CC_ADCREVERSE);
     _SPI_out(CC_COMNORMAL);
   }
  _SPI_out(CC_DISPNORMAL);
  _SPI_out(CC_LCDBIAS9);
  _SPI_out(CC_SETPOWERCTRL+7);
  _SPI_out(CC_BOOSTERRATIO);     _SPI_out(0);
  _SPI_out(CC_REGRESISTOR+7);
  _SPI_out(CC_SETCONTRAST);      _SPI_out(lcd_Contr);
  _SPI_out(CC_STATINDMODE);      _SPI_out(0);
  _SPI_out(CC_DISPON);

  LCD_DISABLE;
}
//-----------------------------------------------------------------------------------------
//- transfers appropiate content of display RAM to page p
void sendPage( uint8_t p ) {
  if ((p>= 0) && (p<lcd_Pages)) {

	//- ein byte einf�hren, welches Flags f�r jede Page enth�lt  (dirty-Flags)
	//- alle Schreiboperationen auf den display buffer setzetn das entsprechende page-dirty-flag
	//- diese routine �bertr�gt nur die ge�nderten pages
	//- und l�scht das dirty-flag entsprechend
	//-
	//- falls Tempo ein Problem wird...

  }
}
//-----------------------------------------------------------------------------------------
//- transfer complete display buffer to display
//- including all pages
void sendPages( void ) {
  uint8_t page, column;

  LCD_ENABLE;
  for (page=0; page<lcd_Pages; page++)
  {
   	LCD_COMMAND;
	_SPI_out(CC_PAGEADR + page);		//Set page address to <page>
	_SPI_out(CC_COLADRH + 0); 			//Set column address to 0 (4 MSBs)
	_SPI_out(CC_COLADRL + 0); 			//Set column address to 0 (4 LSBs)
    LCD_DATA;
	for (column=0; column<lcd_X; column++)
		_SPI_out( disp_ram[page + (column << 3)] );
  }
  LCD_DISABLE;
}
//-----------------------------------------------------------------------------------------


  


//-----------------------------------------------------------------------------------------
//--
//-- exported functions, the interface
//-- 
//--
//-----------------------------------------------------------------------------------------
//- sets / clears a pixel in display buffer
//- does not transfer display buffer to display
//- 
void lcd_Pixel(uint8_t x, uint8_t y, uint8_t pixel)  {
  if ((x<lcd_X) && (y<lcd_Y)) {
	if (pixel != 0) {
			disp_ram[(y >> 3) + (x << 3)] |=  (1 << (y & 0x07));
	} else {
			disp_ram[(y >> 3) + (x << 3)] &= ~(1 << (y & 0x07));
	}
  }
}
//-----------------------------------------------------------------------------------------
void lcd_Init( enum cDispTyp dt, uint8_t topview ) {
  switch (dt) {
    case cDT_DOGM128 : initDOGM128( topview ); break;
	case cDT_DOGM132 : initDOGM132( topview ); break;
	case cDT_DOGL128 : initDOGL128( topview ); break;
  }
  lcd_Clear();
}
//-----------------------------------------------------------------------------------------
//- perfoms a reset at the display controller ST...
void lcd_Reset(void)
{
  SetBit(LCD_CS_PORT,  LCD_CS);
  SetBit(LCD_SCL_PORT, LCD_SCL);
  SetBit(LCD_SI_PORT,  LCD_SI);
  SetBit(LCD_A0_PORT,  LCD_A0);
  ClrBit(LCD_RES_PORT, LCD_RES);
  _delay_us(5);
  SetBit(LCD_RES_PORT, LCD_RES);
}

//-----------------------------------------------------------------------------------------
void lcd_Contrast( unsigned char c)
{
  if (lcd_Contr != c ) {
  	lcd_Contr = c;
  	LCD_ENABLE;
  	LCD_COMMAND;
  	_SPI_out(CC_SETCONTRAST);
  	_SPI_out(c);
  	LCD_DISABLE;
  }
}
//-----------------------------------------------------------------------------------------
//- clears the local RAM and send this to the display
void lcd_Clear( void ) {
  uint16_t d,s;
  s = RAM_SIZE;
  for (d=0; d<s; d++) { disp_ram[d] = 0x00; };     	//- clear display buffer
  sendPages();									 	//- send display buffer to display	
}
//-----------------------------------------------------------------------------------------
void lcd_SetPixel(uint8_t x, uint8_t y) {
	lcd_Pixel(x, y, 1 - g_inverted);
}
//-----------------------------------------------------------------------------------------
void lcd_ClearPixel(uint8_t x, uint8_t y) {
	lcd_Pixel(x, y, 0 + g_inverted);
}
//-----------------------------------------------------------------------------------------
//- transfers display buffer to display
void lcd_Flush( void ) {
  sendPages();
}
//-----------------------------------------------------------------------------------------
uint8_t getBitMask( uint8_t i ) {
  switch(i) {
	case 	0: return( 0x00 );	
	case 	1: return( 0x01 );	//- 00000001
	case 	2: return( 0x03 );	//- 00000011
	case 	3: return( 0x07 );	//- 00000111 
	case 	4: return( 0x0F );	//- 00001111 
	case 	5: return( 0x1F );	//- 00011111 
	case	6: return( 0x3F );	//- 00111111  
	default  : return( 0x7F );	//- 01111111  
  }
}
//-----------------------------------------------------------------------------------------
// places c as a 8-pixel column at position (x,y)
// LSB is at (x,y)
// pix = nr of valid bits in c, beginning with LSB
// needed for text output

void lcd_SetByteCol( uint8_t x, uint8_t y, unsigned char c ) {
  uint16_t  col, row;
  uint8_t   old, a;
  col = x * lcd_Pages;			//- col = starting point in disp_ram[]
  row = y >> 3;					//- row = first offset byte
  a   = y % 8;					//- nr of bits to shift 
  if (a==0) { 					//- byte-aligned
	disp_ram[ col + row ] = c;
  } else {						//- not byte-aligned
  	old = disp_ram[ col + row ];
	setDispRAM( col + row,  (getBitMask( a ) & old) | ( c << a ) );
  	old = disp_ram[ col + row + 1 ];
	setDispRAM( col + row + 1,  (~getBitMask( a ) & old) | ( c >> (8-a)) );
  }
} 
//-----------------------------------------------------------------------------------------
// places c as a 8-pixel row at position (x,y)
// LSB is at (x,y)
// needed for text output
void lcd_SetByteRow( uint8_t x, uint8_t y, unsigned char c ) {

	//- bisher nicht ben�tigt.

} 

//-----------------------------------------------------------------------------------------


//-----------------------------------------------------------------------------------------





