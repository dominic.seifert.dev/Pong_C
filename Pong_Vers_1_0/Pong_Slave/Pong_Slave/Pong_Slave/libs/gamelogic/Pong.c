/*
 * Pong.c
 *
 * Created: 26.12.2014 10:20:21
 *  Author: busser.michael
 */ 


#include <avr/io.h>
#include <util/delay.h>
#include <stdlib.h>
#include <glcd/glcd.h>
#include "sound.h"
#include "timer.h"
#include "adc.h"
#include "Pong.h"

//---------------------------------------------------------------------------------------------------
/**  Zufallszahln im Bereich ug..og erzeugen
*/
uint8_t getRandom( uint8_t ug, uint8_t og ) {
	return(  (rand() % ((og - ug)+1) + ug)  );
}
//---------------------------------------------------------------------------------------------------
void init_Spieler(){
	spieler[SPIELER_A].punkte = 0;
	spieler[SPIELER_A].y = (DIM_Y - PADDLE_Y_SIZE) / 2;		//in der Mitte anfangen

	spieler[SPIELER_B].punkte = 0;
	spieler[SPIELER_B].y = (DIM_Y - PADDLE_Y_SIZE) / 2;		//in der Mitte anfangen
	
	
	gDrawRect(0, spieler[SPIELER_A].y, PADDLE_X_SIZE, PADDLE_Y_SIZE, 1);
	gDrawRect(DIM_X - PADDLE_X_SIZE , spieler[SPIELER_B].y, PADDLE_X_SIZE, PADDLE_Y_SIZE, 1);
}
//---------------------------------------------------------------------------------------------------
void set_Ball_XSpeed( int8_t x) {
	ball.dx = x;
}
//---------------------------------------------------------------------------------------------------
/**
  Die Geschwindigkeit in Y-Richtung als eine Art Prozentwert.
  +1..+10	= Geschwindigkeit, Richtung: DOWN
  0			= kein Anteil in Y-Richtung
  -1..-10	= Geschwindigkeit, Richtung: UP
*/
void set_Ball_YSpeed( int8_t y ) {
	ball.dy = y;	
	ball.delta = abs(y);	//Initialwert
}
//---------------------------------------------------------------------------------------------------
void init_Ball( uint8_t n ) {
	switch (n) {
		case SPIELER_A :ball.x	= BALL_SIZE * 2;				//hier k�nnte man auch eine zuf�llige Position w�hlen
						ball.y	= getRandom( 10, DIM_Y - 10 );	//zuf�llige Starth�he
						set_Ball_XSpeed( +1 );
						set_Ball_YSpeed( -3 );
						break;
						
		case SPIELER_B :ball.x	= DIM_X -(BALL_SIZE * 2);		//hier k�nnte man auch eine zuf�llige Position w�hlen
						ball.y	= getRandom( 10, DIM_Y - 10 );	//zuf�llige Starth�he
						set_Ball_XSpeed( -1 );
						set_Ball_YSpeed( +3 );
						break;
						
		default:		ball.x	= DIM_X / 2;		//hier k�nnte man auch eine zuf�llige Position w�hlen
						ball.y	= DIM_Y / 2;
						set_Ball_XSpeed( +1 );
						set_Ball_YSpeed( 0 );				//keinen Y-Anteil in der Bewegung
	}
	ball.visible	= BALL_INVISIBLE;	//nicht sichbar
}
//---------------------------------------------------------------------------------------------------
void moveBall(){
	if (ball.visible == BALL_INVISIBLE) {	
		gDrawCircle( ball.x, ball.y, BALL_SIZE, 1 );	//Ball erscheinen lassen
		ball.visible = BALL_VISIBLE;
	} else {
		gClearCircle( ball.x, ball.y, BALL_SIZE );		//Ball an alter Position l�schen
		
		//- neue Position bestimmen, dabei die Grenzen beachten	
		//- Horizontale Bewegung:
		if (ball.dx<0) {	//Bewegung nach links
			if (ball.x > abs(ball.dx))	{ ball.x += ball.dx; } else { ball.x = 0; }
		} else {			//Bewegung nach rechts
			ball.x += ball.dx;
			if (ball.x >= DIM_X) { ball.x = DIM_X-1; }
		}

		//Vertikale Bewegung:  kommt nur hinzu, wenn dy von 0 verschieden ist.
		if (ball.dy != 0) {
			
			ball.delta -= abs(ball.dx);
			if (ball.delta <= 0) {		//jetzt ist eine �nderung von Y erforderlich
				if (ball.dy > 0) {		//Richtung:  DOWN
					ball.y++;
					if (ball.y + BALL_SIZE >= DIM_Y) {	//unterer Displayrand erreicht ?
						ball.dy *= -1;	//Vorzeichen umdrehen entspricht reflexion
					}
				} else {				//Richtung: UP
					ball.y--;
					if (ball.y <= BALL_SIZE) {	//Oberer Displayrand erreicht?
						ball.dy *= -1;	//Vorzeichen umdrehen entspricht reflexion
					}
				}
				ball.delta = abs(ball.dy);
			}
			
		}
		//an neuer Position zeichnen
		gDrawCircle( ball.x, ball.y, BALL_SIZE, 1 );	//Ball an neuer Position erscheinen lassen
	}
};
//---------------------------------------------------------------------------------------------------
void clear_Ball() {
  if (ball.visible == BALL_VISIBLE) {
	gClearCircle( ball.x, ball.y, BALL_SIZE );		//Ball an alter Position l�schen
	ball.visible = BALL_INVISIBLE;
  }
}
//---------------------------------------------------------------------------------------------------
/**
  Die Funktion pr�ft, ob die Y-Komponente der Ballposition im Bereich des Paddle eines Spielers liegt
  @param:	s gibt den Spieler an. Erlaubt sind die Werte:   SPIELER_A  und  SPIELER_B
  @return:	1 = ja
			0 = nein
*/
uint8_t touch_Paddle( uint8_t s ) {
	if ((ball.y >= spieler[s].y) &&
		(ball.y <= spieler[s].y + PADDLE_Y_SIZE)) 
		{
			return( 1 );
		} else {
			return( 0 );
		}
}
//---------------------------------------------------------------------------------------------------
void sound_Lost() {
	play_Sound( E1, 70, Sound_Sync );	//Verloren	
	play_Sound( C1, 90, Sound_Sync );	//Verloren
}
//---------------------------------------------------------------------------------------------------
void sound_Bounce() {
	play_Sound( F1, 30, Sound_Sync );	//zur�ck
	//Sound_ASync bietet sich hier an, das scheint aber noch buggy zu sein
	// deshalb bleibt es erst mal bei Soung_Sync
}
//---------------------------------------------------------------------------------------------------
void check_Collisiion() {
	if (ball.dx > 0) {	//Bewegung nach rechts
		if (ball.x > (DIM_X - BALL_SIZE) ) {	// rechte Seite erreicht
			if (touch_Paddle( SPIELER_B )) {
				ball.dx = ball.dx * (-1);		//nur zur�ckspielen
				sound_Bounce();
			} else {
				//MessageBeep();
				clear_Ball();
				sound_Lost();
				if (spieler[SPIELER_A].punkte < 99) { spieler[SPIELER_A].punkte++; }
				//- neuer Ball ins Spiel
				init_Ball(SPIELER_B);
			}
		}
	} else {			//Bewegung nach links
		if (ball.x <= BALL_SIZE ) {				//linke Seite erreicht
			if (touch_Paddle( SPIELER_A )) {
				ball.dx = ball.dx * (-1);		//nur zur�ckspielen
				sound_Bounce();
			} else {
				//MessageBeep();
				clear_Ball();
				sound_Lost();
				if (spieler[SPIELER_B].punkte < 99) { spieler[SPIELER_B].punkte++; }
				//- neuer Ball ins Spiel
				init_Ball(SPIELER_A);
			}
		}
	}
}
//---------------------------------------------------------------------------------------------------
void move_Paddle( uint8_t player ) {
	if (player == SPIELER_A) {
		gClearRect( 0, spieler[player].y, PADDLE_X_SIZE, PADDLE_Y_SIZE );
		spieler[player].y = spieler[player].raw >> 3;
		gDrawRect( 0, spieler[player].y, PADDLE_X_SIZE, PADDLE_Y_SIZE, 1 );
	} else {
		gClearRect( DIM_X - PADDLE_X_SIZE, spieler[player].y, PADDLE_X_SIZE, PADDLE_Y_SIZE );
		spieler[player].y = spieler[player].raw >> 3;
		gDrawRect( DIM_X - PADDLE_X_SIZE, spieler[player].y, PADDLE_X_SIZE, PADDLE_Y_SIZE, 1 );
	}
}
//---------------------------------------------------------------------------------------------------
uint8_t gameOver() {
	return( (spieler[SPIELER_A].punkte >= MAX_PUNKTE) || (spieler[SPIELER_B].punkte >= MAX_PUNKTE) );
}
//---------------------------------------------------------------------------------------------------
void callback_1MS() {
	check_Sound_Async();	
}
//---------------------------------------------------------------------------------------------------
int main(void)
{	char buffer[5];
	
	srand( 0 );					//Zufallszahlengenerator initislisieren
	
	_delay_ms( 20 );
	LCD_DDR = LCD_DDR_VAL;     	//- Bits f�r LCD-Display am PortB auf Ausgang

	//- Grafikdisplay initialisieren -------------------------------------------------
	gInit();

	init_Ball( NEW_BALL );
	set_Ball_YSpeed( -5 );
	
	init_Sound();
	
	register_Timer_Callback_1MS( &callback_1MS );
	timerInit();	//ben�tigt f�r das asynchrone Ausschalten der Tonausgabe
	
	init_adc();

    while(1)
    {

		init_Spieler();

		gClear();
		//- Startbildschirm anzeigen
		fontPutString( &fontTimes16, "Pong", 40, 20 );
		fontPutString( &fontTimes10, "V0.1", 60, 40 );
		gFlush();
		_delay_ms(2000);

		gClear();
		gFlush();
		
		//- warte auf Tastendruck zum Start
		//- bisher einfach 2 Sek. warten

		//- Spieleschleife
		do {
			//Position der Paddles abfragen
			spieler[SPIELER_A].raw = read_adc( SPIELER_A );
			spieler[SPIELER_B].raw = read_adc( SPIELER_B );
		
			//Position der Paddles einstellen    
			move_Paddle( SPIELER_A );
			move_Paddle( SPIELER_B );
		
			//Ball einen Schritt bewegen
			moveBall();
		
			//Kollisionspr�fung
			check_Collisiion();
		
			//Punkte darstellen
			itoa( spieler[SPIELER_A].punkte, buffer, 10 );
			fontPutString( &fontTimes10, "   \0", 0,0 );
			fontPutString( &fontTimes10, buffer, 0,0 );

			itoa( spieler[SPIELER_B].punkte, buffer, 10 );
			fontPutString( &fontTimes10, "   \0", 115,0 );
			fontPutString( &fontTimes10, buffer, 115,0 );
		
			//Bild ausgeben
			gFlush();
		
		} while (!gameOver());
		
		//- Game Over - bildschirm anzeigen inkl. Punktestand
		gClear();
		fontPutString( &fontTimes16, "Game over", 40, 30 );
		gFlush();
		_delay_ms(5000);

		
		//- Warte ein wenig		
    }
}