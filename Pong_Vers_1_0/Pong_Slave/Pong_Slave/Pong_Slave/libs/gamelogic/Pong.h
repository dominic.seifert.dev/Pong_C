/*
 * Pong.h
 *
 * Created: 26.12.2014 10:22:24
 *  Author: busser.michael
 */ 


#ifndef PONG_H_
#define PONG_H_

#define DIM_X				128			//Bildschirmgr��e in X-Richtung, Z�hlung beginnt bei 0
#define DIM_Y				64			//Bildschirmgr��e in Y-Richtung, Z�hlung beginnt bei 0

#define SPIELER_A			0
#define SPIELER_B			1
#define NEW_BALL			2
#define AKT_SPIELER			SPIELER_A

#define PADDLE_Y_SIZE		15			//H�he des Schl�gers, Pixel in Y-Richtung
#define PADDLE_X_SIZE		2			//Breite des Schl�gers, Pixel in X-Richtung
#define PADDLE_X_POS_A		0
#define PADDLE_X_POS_B		DIM_X - PADDLE_X_SIZE - 1
#define PADDLE_X_SIZE		2

#define BALL_SIZE			4			//radius! 5 px
#define BALL_MOVE_UP		0
#define BALL__MOVE_DOWN		1
#define BALL_INVISIBLE		0
#define BALL_VISIBLE		1

#define MAX_PUNKTE			10

typedef struct {
	uint8_t		punkte;		//Punktestand
	uint8_t		y;			//Y-Posisiton des Schl�gers
	uint16_t	raw;		//Rohwert des ADC vor der Umrechnung auf Schl�gerposition
} TSpieler;


typedef struct {
	uint8_t	x;				//akt. x,y Koordinaten des Balles
	uint8_t	y;	
	int8_t	dx;				//Bewegungskomponente in X-Richtung		>0: rechts  <0: links   =0: Stillstand
	int8_t	dy;				//Bewegungskomponente in Y-Richtung		-100..0..+100	<=: nach oben,	>0:nach unten,   =0:Stillstand	
	int8_t	delta;			//zur Berechnung der Y-Komponente
	uint8_t	visible;		//Ball ist sichbar oder nicht			0=unsichtbar	1=sichtbar
} TBall;

TSpieler spieler[2];	//die beiden Spieler als Feld
TBall	 ball;			//des Ball als record-Variable

#endif /* PONG_H_ */