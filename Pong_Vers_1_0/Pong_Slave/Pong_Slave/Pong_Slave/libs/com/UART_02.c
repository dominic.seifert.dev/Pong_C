/*
 * UART_02.c
 *
 * Created: 30.08.2015 12:31:29
 *  Author: busser.michael
 */ 


#include <avr/io.h>
#include <avr/interrupt.h>		//Interrupts einbinden
#include <util/delay.h>

#include "timer.h"
#include "serial.h"				//nur wg. initSerial()
#include "protocol.h"
#include "key.h"

uint8_t[] slaveAdresses = new uint8_t[2];

void sendSlaveAdress(){
	
	TDataBuf msg;
	
	msg.Datagramm.src = ownAdr();
	msg.Datagramm.dst = masterAdr();
	msg.Datagramm.mt  = MT_DISPLAY_REQ;
	
	msg.Datagramm.mode.bAckReq	= NO_ACK_REQUIRED;
	msg.Datagramm.mode.bCRC		= NO_CHECKSUM;		//WITH_CHECKSUM;
	msg.Datagramm.mode.count	= 11;				// die Header- und Trailerbytes hier nicht mitz�hlen

	char[] slaveAdress = msg.Datagramm.src; 
	
	msg.Datagramm.Payload[0] = 'S';
	msg.Datagramm.Payload[1] = 'L';
	msg.Datagramm.Payload[2] = msg.Datagramm.src;
	msg.Datagramm.Payload[3] = '0';
	msg.Datagramm.Payload[4] = '0';
	msg.Datagramm.Payload[5] = '0';
	msg.Datagramm.Payload[6] = '0';
	msg.Datagramm.Payload[7] = '0';
	
	msgPrepare( &msg );
	sendMsg( msg );
}

void sendSlaveMsgUP(){

	TDataBuf msg;
	
	msg.Datagramm.src = ownAdr();
	msg.Datagramm.dst = masterAdr();
	msg.Datagramm.mt  = MT_DISPLAY_REQ;
	
	msg.Datagramm.mode.bAckReq	= NO_ACK_REQUIRED;
	msg.Datagramm.mode.bCRC		= NO_CHECKSUM;		//WITH_CHECKSUM;
	msg.Datagramm.mode.count	= 11;				// die Header- und Trailerbytes hier nicht mitz�hlen

	msg.Datagramm.Payload[0] = 'S';
	msg.Datagramm.Payload[1] = 'L';
	msg.Datagramm.Payload[2] = msg.Datagramm.src;
	msg.Datagramm.Payload[3] = 'U';
	msg.Datagramm.Payload[4] = 'P';
	msg.Datagramm.Payload[5] = '0';
	msg.Datagramm.Payload[6] = '0';
	msg.Datagramm.Payload[7] = '0';
	
	msgPrepare( &msg );
	sendMsg( msg );
}

	
void sendSlaveMsgDown(){

	TDataBuf msg;
	
	msg.Datagramm.src = ownAdr();
	msg.Datagramm.dst = masterAdr();
	msg.Datagramm.mt  = MT_DISPLAY_REQ;
	
	msg.Datagramm.mode.bAckReq	= NO_ACK_REQUIRED;
	msg.Datagramm.mode.bCRC		= NO_CHECKSUM;		//WITH_CHECKSUM;
	msg.Datagramm.mode.count	= 11;				// die Header- und Trailerbytes hier nicht mitz�hlen

	msg.Datagramm.Payload[0] = 'S';	// Gesendet von Slave
	msg.Datagramm.Payload[1] = 'L';
	msg.Datagramm.Payload[2] = slaveAdress; 
	msg.Datagramm.Payload[3] = 'D'; // Richtung des Panels
	msg.Datagramm.Payload[4] = 'W';
	msg.Datagramm.Payload[5] = '0'; // Punkte
	msg.Datagramm.Payload[6] = '0';
	msg.Datagramm.Payload[7] = '0';
	
	msgPrepare( &msg );
	sendMsg( msg );
}

void sendMasterMsgStart(uint8_t  flag){
// flag global setzen
	if(flag == 1){
		TDataBuf msg;

		msg.Datagramm.dst = masterAdr();
		msg.Datagramm.dst = slaveAdresses[0];
		msg.Datagramm.mt  = MT_DISPLAY_REQ;

		msg.Datagramm.mode.bAckReq	= NO_ACK_REQUIRED;
		msg.Datagramm.mode.bCRC		= NO_CHECKSUM;		//WITH_CHECKSUM;
		msg.Datagramm.mode.count	= 11;				// die Header- und Trailerbytes hier nicht mitz�hlen

		msg.Datagramm.Payload[0] = 'M';
		msg.Datagramm.Payload[1] = 'A';
		msg.Datagramm.Payload[2] = msg.Datagramm.src;
		msg.Datagramm.Payload[3] = 'l';
		msg.Datagramm.Payload[4] = '0';
		msg.Datagramm.Payload[5] = ' ';
		msg.Datagramm.Payload[6] = 'W';
		msg.Datagramm.Payload[7] = 'e';
		
		msgPrepare( &msg );
		sendMsg( msg );
		flag++;	
	} else if(flag == 2)

	
	
}

void sendMasterMsgStop(){

	TDataBuf msg;

	msg.Datagramm.dst
	msg.Datagramm.dst = masterAdr();
	msg.Datagramm.mt  = MT_DISPLAY_REQ;

	msg.Datagramm.mode.bAckReq	= NO_ACK_REQUIRED;
	msg.Datagramm.mode.bCRC		= NO_CHECKSUM;		//WITH_CHECKSUM;
	msg.Datagramm.mode.count	= 11;				// die Header- und Trailerbytes hier nicht mitz�hlen
	
	msg.Datagramm.Payload[0] = 'M';
	msg.Datagramm.Payload[1] = 'A';
	msg.Datagramm.Payload[2] = msg.Datagramm.dst;
	msg.Datagramm.Payload[3] = 'S';
	msg.Datagramm.Payload[4] = 'T';
	msg.Datagramm.Payload[5] = '0';
	msg.Datagramm.Payload[6] = '0';
	msg.Datagramm.Payload[7] = '0';

	msgPrepare( &msg );
	sendMsg( msg );	
		
}

void sendTestMsg( void ) {
	
	TDataBuf msg;
	
	msg.Datagramm.src = ownAdr();
	msg.Datagramm.dst = masterAdr();
	msg.Datagramm.mt  = MT_DISPLAY_REQ;
	
	msg.Datagramm.mode.bAckReq	= NO_ACK_REQUIRED;
	msg.Datagramm.mode.bCRC		= NO_CHECKSUM;		//WITH_CHECKSUM;
	msg.Datagramm.mode.count	= 11;				// die Header- und Trailerbytes hier nicht mitz�hlen

	msg.Datagramm.Payload[0] = 'H';
	msg.Datagramm.Payload[1] = 'a';
	msg.Datagramm.Payload[2] = 'l';
	msg.Datagramm.Payload[3] = 'l';
	msg.Datagramm.Payload[4] = '0';
	msg.Datagramm.Payload[5] = ' ';
	msg.Datagramm.Payload[6] = 'W';
	msg.Datagramm.Payload[7] = 'e';
	msg.Datagramm.Payload[8] = 'l';
	msg.Datagramm.Payload[9] = 't';
	msg.Datagramm.Payload[10] = '!';
	msgPrepare( &msg );
	sendMsg( msg );
}

void sendKeyMsg( uint8_t tasten ) {
	
	TDataBuf msg;
	
	msg.Datagramm.src = ownAdr();
	msg.Datagramm.dst = masterAdr();
	msg.Datagramm.mt  = MT_KEY_REQ;
	
	msg.Datagramm.mode.bAckReq	= NO_ACK_REQUIRED;
	msg.Datagramm.mode.bCRC		= NO_CHECKSUM;		//WITH_CHECKSUM;
	msg.Datagramm.mode.count	= 1;				// die Header- und Trailerbytes hier nicht mitz�hlen

	msg.Datagramm.Payload[0]	= tasten;
	msgPrepare( &msg );
	sendMsg( msg );
}

int main(void)
{
	uint8_t tasten;
	
	timerInit();
	initSerial();			//
	key_init();				//
	
	sei();
	sendTestMsg();
	
    while(1)
    {
        //TODO:: Please write your application code 
		
		if (timerFlags.flags.bCenti) {
			getKeyState();					//Tastenabfragen
			timerClearCenti();
		}

		tasten = get_key_press(ALL_KEYS);
		if ( tasten ) {
			sendKeyMsg( tasten);	
		}

		if (timerFlags.flags.bSek) {	//Das Flag wird 1x pro Sekunde vom Timer gesetzt
		//	sendTestMsg();
			timerClearSek();
		}
		
		
    }
}