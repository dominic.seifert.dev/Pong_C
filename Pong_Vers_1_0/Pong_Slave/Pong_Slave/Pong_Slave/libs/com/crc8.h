/*
 * crc8.h
 *
 * Der hier verwendete Code geht auf ein Beispiel von Harry May zur�ck.
 *  http://www.mikrocontroller.net/topic/155115
 * 
 * Die Headerinformation aus dem C-Hile ist hier wiedergegeben. 
 * Ich habe das ganze nur nur meine Zwecke angepasst.
 *
 *
 * CRC-8 Calculation
 * =================
 * 
 * erstellt von: Kurt Moraw (Juli 2009) www.helitron.de 
 * 
 *  Grundlagen zu diesen Funktionen wurden der Webseite:
 *  http://www.cs.waikato.ac.nz/~312/crc.txt
 *  entnommen (A PAINLESS GUIDE TO CRC ERROR DETECTION ALGORITHMS)
 * 
 *  Das Ergebnis wurde geprueft mit dem CRC-Calculator:
 *  http://www.zorc.breitbandkatze.de/crc.html
 * 
 *  Das Generator-Polynom wurde wie folgt gewaehlt:
 *  Polynom = x^8+x^7+x^6+x^4+x^2+1 = x^8+x^7+x^6+x^4+x^2+X^0
 *  das entspricht: (1) 1101 0101 = Hex d5
 * 
 *  bis zu einer maximalen Bitlaenge von 93 entspricht die Datensicherungsfunktion der Hamming-Distanz 4
 *
 *  crc8_bytecalc
 *
 * diese Funktion enthaelt die CRC8 Schleife fuer 8 Bit einer Nachricht.
 * Sie wird solange aufgerufen bis die komplette Nachricht verarbeitet wurde,
 * danach muss sie nochmals mit dem Wert 0 aufgerufen werden um die CRC-8 Berechnung abzuschliessen
 *
 * Parameter: byte ... ein Byte der Nachricht
 * Return: aktueller Wert des CRC-8
 *
 *	
 *
 * Created: 16.10.2015 18:55:54
 *  Author: busser.michael
 */ 


#ifndef CRC8_H_
#define CRC8_H_

uint8_t crc8_messagecalc(uint8_t *msg, uint8_t len);


#endif /* CRC8_H_ */