#ifndef SOUND_H_
#define SOUND_H_



/**
	Eine kleine Bibliothek, um mit Hilfe eines Timers und der Pulsweitenmodulation T�ge zu erzeugen.
	Die Frequenz bestimmt die Tonh�he, das Tastverh�ltnis die Lautst�rke:
	
	Verwendete Resourcen:
	Timer 1
	PD5 als Ausgang erforderlich, steuert den Beeper an
	
	//- als Beispiel die Tonleiter:
	
	ton_an(C1);	_delay_ms(250);	ton_aus();
	ton_an(D1);	_delay_ms(250);	ton_aus();
	ton_an(E1);	_delay_ms(250);	ton_aus();
	ton_an(F1);	_delay_ms(250);	ton_aus();
	ton_an(G1);	_delay_ms(250);	ton_aus();
	ton_an(A1);	_delay_ms(250);	ton_aus();
	ton_an(H1);	_delay_ms(250);	ton_aus();
	ton_an(C2);	_delay_ms(250);	ton_aus();	
	
*/	

#define C1 1911*2
#define D1 1703*2
#define E1 1517*2
#define F1 1432*2
#define G1 1275*2
#define A1 1136*2
#define H1 1012*2
#define C2 956*2

#define Sound_Sync		0
#define Sound_ASync		1


uint16_t	playDelay;	//Verz�gerungszeit f�r die asynchrone Tonsteuerung


//- public
void init_Sound( );
void play_Sound( uint16_t ton, uint16_t delayms, uint8_t sync );
void check_Sound_Async();

#endif	/* SOUND_H_ */