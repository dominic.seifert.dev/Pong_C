﻿


#include <avr/io.h>
#include <util/delay.h>
#include "sound.h"



//-------------------------------------------------------------------------
void ton_an( uint16_t ton) {
	TCCR1A	|= (1<<COM1A1);				//Clear OC1A/OC1B on compare match (Set output to low level)
	TCCR1B	|= (1<<CS11) | (1<<WGM13);	//Prescaler 8, PWM: Phase and Frequency Correct
										//Wert, bis zu dem gezählt wird, steht in ICR1
	OCR1AH	= 0;		//Wert, bei dem ein Output-Match-Ereginis stattfindet, das zur Umkehr des Schaltwertes von OC1A führt
	OCR1AL	= 60;
	ICR1	= ton;
}

//-------------------------------------------------------------------------
void ton_aus() {
	TCCR1B	&= ~(1<<CS11);	//Timer/Counter stopp
}

//-------------------------------------------------------------------------
void delay_ms(uint16_t n) {
	while (n--) {
		_delay_ms(1);
	}
}
//-------------------------------------------------------------------------
void init_Sound( void ) {
	DDRD |= (1<<PD5);		//Ausgang
}

//-------------------------------------------------------------------------
void play_Sound( uint16_t ton, uint16_t delayms, uint8_t sync ) {
	if (sync == Sound_Sync) {
		ton_an( ton );
		delay_ms( delayms );
		ton_aus();
	} else {
		ton_an( ton );
		playDelay = delayms;
	}
}
//-------------------------------------------------------------------------
/** 
  Muss regelmässig im 1ms-Intervall aufgerufen werden, damit das Timing stimmt und 
  das asynchrone Ausschalten eines Tones klappen kann.
*/
void check_Sound_Async(){
	if (playDelay > 0) {
		playDelay--;
		if (playDelay == 0) { ton_aus(); }
	}
}
