/*  

Headerfile f�r den Treiber zur Bibliothek mit 
Ansteuerroutinen f�r grafische Displays

Controllertyp:  ST7565


*/

#ifndef GLCD_DRIVER_ST7565
#define GLCD_DRIVER_ST7565


/*------------------------------------------------------------------------------
   Commandcodes f�r ST7565
------------------------------------------------------------------------------*/

#define CC_REGRESISTOR  0x20
#define CC_SETPOWERCTRL 0x28
#define CC_SETCONTRAST  0x81
#define CC_DISPOFF      0xAE
#define CC_DISPON       0xAF
#define CC_DISPSTART    0x40
#define CC_PAGEADR      0xB0
#define CC_COLADRL      0x00
#define CC_COLADRH      0x10
#define CC_ADCNORMAL    0xA0
#define CC_ADCREVERSE   0xA1
#define CC_LCDBIAS9     0xA2
#define CC_LCDBIAS7     0xA3
#define CC_DISPNORMAL   0xA6
#define CC_DISPREVERSE  0xA7
#define CC_STATINDMODE  0xAC
#define CC_COMNORMAL    0xC0
#define CC_COMREVERSE   0xC8
#define CC_RESET        0xE2
#define CC_BOOSTERRATIO 0xF8


/*------------------------------------------------------------------------------
 Portdefinitionen
 SPI mit normalen Portleitungen ohne die SPI-Hardware zu verwenden
------------------------------------------------------------------------------*/

#define LCD_CS_PORT		PORTB
#define LCD_CS			1

#define LCD_RES_PORT	PORTB
#define LCD_RES			0

#define LCD_A0_PORT		PORTB
#define LCD_A0			2

#define LCD_SCL_PORT	PORTB
#define LCD_SCL			4

#define LCD_SI_PORT		PORTB
#define LCD_SI			3


#define LCD_DDR			DDRB	
#define LCD_DDR_VAL		((1<<LCD_CS) | (1<<LCD_RES) | (1<<LCD_A0) | (1<<LCD_SCL) | (1<<LCD_SI))


/*------------------------------------------------------------------------------
 Makrodefinitionen
------------------------------------------------------------------------------*/

#define SetBit(adr, bnr)	( (adr) |=  (1 << (bnr)) )
#define ClrBit(adr, bnr)	( (adr) &= ~(1 << (bnr)) )

#define	LCD_ENABLE  ClrBit(LCD_CS_PORT, LCD_CS)
#define	LCD_DISABLE SetBit(LCD_CS_PORT, LCD_CS)

#define	LCD_COMMAND ClrBit(LCD_A0_PORT, LCD_A0)
#define	LCD_DATA    SetBit(LCD_A0_PORT, LCD_A0)
//#define RAM_SIZE    (LCD_LINES * LCD_LINE_LENGTH) >> 3 ;    //- kann der Preprozessor das nicht aufl�sen ??
#define RAM_SIZE    1024;

/*------------------------------------------------------------------------------
  LCD spec definitions in global variables
  will be defined with call to init*
------------------------------------------------------------------------------*/


//- Will be defined by call to init*
uint8_t		lcd_Contr;					//- actual Contrast
uint8_t		lcd_X;						//- nr of columns
uint8_t		lcd_Y;						//- nr of rows
uint8_t		lcd_Pages; 					//- nr of pages
uint8_t		lcd_X_offs;					//- 
uint8_t 	g_inverted;					// Invert all drawing functions


/*------------------------------------------------------------------------------
 Datatypes
------------------------------------------------------------------------------*/

enum cDispTyp { cDT_DOGM128, cDT_DOGM132, cDT_DOGL128 };

/*------------------------------------------------------------------------------
 Bildspeicher im RAM
 erfordert eine Controller mit mehr als 1 kByte RAM
------------------------------------------------------------------------------*/

uint8_t disp_ram[ 1024 ];		//- entsprechend RAM_SIZE

/*------------------------------------------------------------------------------
 Funktionsprototypen
 der Routinen, die der Treiber exportiert.
------------------------------------------------------------------------------*/

void lcd_Init( enum cDispTyp dt, uint8_t topview ); 			//- Set display type and orientation 
void lcd_Reset(void);											//- Reset the Displaycontroller
void lcd_Contrast( unsigned char c);							//- Set a new value for Contrast
void lcd_Clear(void);											//- Clear Display RAM
void lcd_Pixel(uint8_t x, uint8_t y, uint8_t pixel);			//- Sets or clears a Pixel at x,y due to the value of pixel
void lcd_SetPixel(uint8_t x, uint8_t y);						//- Set a pixel at x,y in display buffer, no transfer to display
void lcd_ClearPixel(uint8_t x, uint8_t y);						//- Clears a pixel at x,y in display buffer, no transfer to display
void lcd_Flush( void );											//- transfers display buffer to display
void lcd_SetByteCol( uint8_t x, uint8_t y, unsigned char c );	//- places c as a 8-pixel column at position (x,y) where LSB is at (x,y)


#endif //GLCD_DRIVER_ST7565
