#include <avr\io.h>
#include <avr\interrupt.h>
#include <inttypes.h>

#include "protocol.h"
#include "crc8.h"

/**
@defgroup library Bibliothek
@{
*/


/**
@defgroup protocol ein einfaches Protokoll
@{
*/



/************************************************************************/
/*   Protocol.c              
/*   Adressverwaltung        
/*   Problemchecker                        
/*                           
/*                           
/************************************************************************/*/




/*---------------------------------------------------------------- 
  interne Variablen
----------------------------------------------------------------*/
/**
@brief	Interne Variablen.
		
@return			
*/

//TDataBuf	recbuf;								//- Empfangspuffer f�r eine Nachricht ohne STX / ETX  aber mit CS
//TDataBuf	sendbuf;							//- Puffer f�r die zu sendende Nachricht, Das Senden selbst l�uft Interruptgesteuert



//---------------------------------------------------------------------
/**
@brief	Liefert die eigene Adresse dieses Knotens.
		Die Daten stammen typischerweise aus dem EEPROM des Controllers.
		Hier zun�chst einen festen Wert zur�ckliefern.
		
@return	Adresse dieses Knotens		
*/
uint8_t ownAdr() {
	return( 0x05 );
}
//---------------------------------------------------------------------
/**
@brief	Liefert die Adresse f�r den Master eines Bussystems.
		Das Braucht man, wenn ein Knoten spontan Nachrichten verwenden soll.
		Sonst gehen die antworten immer an den Sender zur�ck.
		
		Die Daten stammen typischerweise aus dem EEPROM des Controllers.
		Hier zun�chst einen festen Wert zur�ckliefern.
		
@return	Adresse des Busmasters		
*/
uint8_t masterAdr() {
	return( 0x0A );
}
//---------------------------------------------------------------------
/**
@brief	L�scht die �bergebene Nachricht und setzt damit alle Felder auf 0.
		
@return	die gel�schte Nachricht		
*/
void msgClear( TDataBuf *msg ) {    
  for( uint8_t i=0; i<MSG_LEN_MAX; i++) {
    (*msg).Buf[i] = 0;
  } 
}

//---------------------------------------------------------------------
uint8_t getPayloadLen( TDataBuf *msg ) {
	uint8_t res;
	res = (*msg).Datagramm.mode.count & 0x0F;
	return( res );
}
//---------------------------------------------------------------------
/**
@brief	Berechnet die Pr�fsumme f�r das Feld cs und setzt den erforderlichen Wert.
		Die Berechnung der Pr�fsumme enth�lt alle Felder der Nachricht au�er dem Feld cs selbst.
		Die Elemente des Frame, also STX und ETX werden ebenfalls nicht mit ber�cksichtigt
		
@return	Der Wert im Feld cs und das Flag bNoCRC im Feld Mode		
*/
//---------------------------------------------------------------------
void setCRC( TDataBuf *msg ) {
	uint8_t x;

	//(*msg).Datagramm.mode.bCRC  = 0;               //solange wir noch keine Pr�fsumme berechnen
	//(*msg).Datagramm.cs = NO_CHECKSUM;
	
	x = crc8_messagecalc( (*msg).Buf, MSG_HEADER_LEN + getPayloadLen(msg) );
	(*msg).Datagramm.mode.bCRC  = x;            //die berechnete Pr�fsumme setzen
	(*msg).Datagramm.cs = WITH_CHECKSUM;		//und Fals korrigieren		
}


//---------------------------------------------------------------------
/**
@brief	Berechnet die Pr�fsumme aus der Nachricht msg und vergleicht diese mit dem Wert in cs.
		
@return	0 = Pr�fsumme nicht korrekt
		1 = Pr�fsumme korrekt oder keine Pr�fsumme enthalten (bCRC=1)
*/

uint8_t checkCRC( TDataBuf *msg ) {
  if ( (*msg).Datagramm.mode.bCRC == 0 ) {
	  return(1);
  } else {
	  //TODO:  sobsld eine Pr�fsumme berechnet wird, die Funktion und den Vergleich hier einf�gen
	  return(1);
  }
}



/**
@brief	Bereitet eine Nachricht zum Versand vor
		Es wird die Pr�fsumme berechnet, sofert dies im Header angeforder wurde,
		ansonsten wird da sFeld cs auf 0 gesetzt
		
@return	keine
		
*/
void msgPrepare( TDataBuf *msg ) {
	
	if ( (*msg).Datagramm.mode.bCRC	) {
		setCRC( msg );
	} else {
		(*msg).Datagramm.mode.bCRC  = 0; 
	}
	
	
}



/**@}*/


/**@}*/








