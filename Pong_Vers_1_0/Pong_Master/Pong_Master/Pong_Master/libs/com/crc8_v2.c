/* CRC-8 Calculation
   =================

erstellt von: Kurt Moraw (Juli 2009) www.helitron.de

Grundlagen zu diesen Funktionen wurden der Webseite:
http://www.cs.waikato.ac.nz/~312/crc.txt
entnommen (A PAINLESS GUIDE TO CRC ERROR DETECTION ALGORITHMS)

Das Ergebnis wurde geprueft mit dem CRC-Calculator:
http://www.zorc.breitbandkatze.de/crc.html

Das Generator-Polynom wurde wie folgt gewaehlt:
Polynom = x^8+x^7+x^6+x^4+x^2+1 = x^8+x^7+x^6+x^4+x^2+X^0
das entspricht: (1) 1101 0101 = Hex d5

bis zu einer maximalen Bitlaenge von 93 entspricht die Datensicherungsfunktion der Hamming-Distanz 4
*/

/* crc8_bytecalc

diese Funktion enthaelt die CRC8 Schleife fuer 8 Bit einer Nachricht.
Sie wird solange aufgerufen bis die komplette Nachricht verarbeitet wurde,
danach muss sie nochmals mit dem Wert 0 aufgerufen werden um die CRC-8 Berechnung abzuschliessen

Parameter: byte ... ein Byte der Nachricht
Return: aktueller Wert des CRC-8
*/

#include <stdio.h>

unsigned char reg=0;	// Rechen-Register fuer den CRC Wert mit Initial Value 0

unsigned char crc8_bytecalc(unsigned char byte)
{
int i;				// Schleifenzaehler
char flag;			// flag um das oberste Bit zu merken
unsigned char polynom = 0xd5;	// Generatorpolynom

	// gehe fuer jedes Bit der Nachricht durch
	for(i=0; i<8; i++) {
		if(reg&0x80) flag=1; else flag=0;	// Teste MSB des Registers
		reg <<= 1;							// Schiebe Register 1 Bit nach Links und
		if(byte&0x80) reg|=1;				// Fülle das LSB mit dem naechsten Bit der Nachricht auf	
		byte <<= 1;							// nächstes Bit der Nachricht
		if(flag) reg ^= polynom;			// falls flag==1, dann XOR mit Polynom
	}
	return reg;
}

/* crc8_messagecalc

Ruft crc8_bytecalc solange auf bis die Nachricht vollstaendig abgearbeitet wurde, danach
wird crc8_bytecalc noch einmal mit Wert 0 aufgerufen um die CRC-8 Berechnung abzuschliessen

Parameter: 
msg ... Bytearray welches die Nachricht enthaelt
len ... Laenge der Nachricht
Return: CRC-8 der kompletten Nachricht
*/

unsigned char crc8_messagecalc(unsigned char *msg, int len)
{
int i;
	for(i=0; i<len; i++) {
		crc8_bytecalc(msg[i]);			// Berechne fuer jeweils 8 Bit der Nachricht
	}
	return crc8_bytecalc(0);			// die Berechnung muss um die Bitlaenge des Polynoms mit 0-Wert fortgefuehrt werden
}


/*
main()
{
unsigned char crc8;						// Ergebnis
unsigned char message[20] = {"AB12"};	// beliebige Nachricht

	reg = 0;							// Init Rechenregister
	crc8 = crc8_messagecalc(message,4);
	printf("CRC-8 ist %x\n",crc8);
}

*/